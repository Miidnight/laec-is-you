# LAEC IS YOU

> `BABA IS YOU` + `L'Avenir En Commun` + `⬡`


## PLAY IS FUN

    NUMPAD IS MOVE
    ARROW IS MOVE
    MOUSE IS MOVE
    ENTER IS ENTER
    ESCAPE IS ESCAPE
    SPACE IS PASS
    R IS RESTART
    U IS UNDO


## GAME IS NOT DONE

You're welcome to hack/fix any of these, as well as to add levels or art.

- [x] YOU
- [x] WIN
- [x] STOP
- [x] PUSH
- [x] DEFEAT
- [x] SINK
- [x] OPEN
- [x] SHUT
- [x] WEAK
- [x] HOT
- [x] MELT
- [x] AND
- [x] NOT
- [x] HAS
- [ ] PULL
- [ ] MORE
- [ ] MOVE
- [ ] TELE
- [ ] FACING
- [ ] LONELY
- [ ] MAKE
- [ ] NEAR
- [ ] ON
- [ ] PANIC
- [ ] BLUE
- [ ] WHITE
- [ ] RED
- [ ] GROUP
- [ ] TEXT
- [ ] FLOAT (need help from expert)
- [ ] EMPTY
- [ ] …
- [ ] Localization in French
    - [x] `PO` file
    - [x] Auto-detect Locale (needs testing)
    - [ ] Locale Picker
- [ ] More levels
- [ ] Music
    - [ ] Start Music
    - [ ] Game Music
    - [ ] Limbo Music
- [ ] Settings
    - [x] Remap movement inputs
    - [ ] Remap level control inputs
    - [x] Sound volume
- [ ] Multiple save slots
- [ ] Gamepad controls
- [ ] Mouse controls
    - [x] Drag Joystick (mouse drag)
    - [x] Undo on right click
    - [ ] Dark Melaffee (draw, move on release)
    - [ ] Clickable hexagon in a corner
- [ ] Touch controls (swipe)
- [ ] Menus
    - [x] Start Menu
    - [x] Settings Menu
    - [ ] About Menu
    - [ ] Pause Menu



## GOD IS YOU

_You make the game._

1. Download [Godot 3](https://godotengine.org/download/),
2. Download [the source](https://framagit.org/laec-is-you/laec-is-you/-/archive/master/laec-is-you-master.zip) and unzip it somewhere.
3. Run Godot.
4. Open this project.
5. ???
6. Enjoy!

> Remember to request a merge of your additions, so that everyone can enjoy them!


### YOU MAKE LEVEL

Go to `Scene > New inherited scene`, choose `core/Level.tscn`.

In the Inspector, on the right, select a map scene, and set a title and subtitle.

Select the `Items` layer node, right-click on it, and instance a child scene from `core/Item.tscn`.

You can duplicate the Item child scenes, change their concepts,
move them around, and hit REFRESH in the top dock of the 2D Editor.


Save your scene somewhere in `levels/`.


---

You can also make a new map, or submaps.


### YOU MAKE ART

Things are in the directory `sprites/items`.


### BUG IS MORE

This project is the product of a quick jam from amateurs.  **There will be bugs.**

Some will be hairy.  Some will be icky.
As a level designer, you will see them all.  Brace yourselves!

Report them, fix them, bypass them…   Be creative, and most importantly: _DON'T PANIC_.


#### PIT IS FALL

- `NOT THING IS NOT THING`
  → should blow up pretty quick, since it will create many items on each turn


## CONFIG IS OPEN

Godot allows overriding any part of our `project.godot` file, including (but not limited to) the actions shortcuts.

Create an `override.cfg` file with the parts of the `project.godot` you wish to verride, and put it right next to the distributed binary.

Refer to the [Godot documentation](https://docs.godotengine.org/fr/stable/classes/class_projectsettings.html) about this feature.


## CODE IS CLOSE

_Just kidding._  The code is **WTFPL**.
That means you may do _whatever the fork you want_ with it, _except complain_.
The license of the assets is CC0.
Godot Game Engine is MIT.


## CONTRIBUTORS HAS LOVE

> [Le Discord Insoumis](https://help_me_I_cannot_link)

- Fanelia (💃🎨) _Art design_
- Domi41 (⌨👽) _Code design_
- Prechurius (🎶🎹) _Sound design_
- Adrenesis (🎅🎮) _Editor design_
- Miidnight (🌿🌈) _Coordination_
- Dazo (🌋🎊) _Game design_
- Bogossdu69 (💀❤) _Silent support_
- Sylvain (🍇🦄) _Mastermind_
- Zargett (👮🕊) _Level Design_


## YOU HAS THANK

Mostly people who unwittingly contributed to the project.

- Godot Game Engine Contributors
- Kenney (CC0 assets from https://kenney.nl/)
- La Riot Insoumise

