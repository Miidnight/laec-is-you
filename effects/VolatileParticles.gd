extends CPUParticles2D


# Autodetachable CpuParticles2D emitter.


var ephemereal_delay := 0.1

var __is_ephemereal := false


# Only set in original/ emittor
var __parent : Node2D
var __grandparent : Node2D


# Only set in ephemereal emittor
var __shepherd : Node2D


func _process(delta):
	process(delta)


func process(_delta):
	if is_emitting() and has_shepherd():
		try_following_shepherd()


func restart():
	var err = assert_scene_conformity()
	if __is_ephemereal or OK != err:
		.restart()
	else:
		yield(get_tree().create_timer(ephemereal_delay), "timeout")
		
		var ephemereal = self.duplicate(true)
		ephemereal.set_script(get_script())
		ephemereal.mark_as_ephemereal()
		ephemereal.start_following(__parent)
		ephemereal.try_following_shepherd()
		__grandparent.add_child(ephemereal)
		ephemereal.restart()
		
		yield(get_tree().create_timer(self.lifetime), "timeout")
		ephemereal.queue_free()


func mark_as_ephemereal() -> void:
	__is_ephemereal = true


func assert_scene_conformity():
	__parent = get_parent()
	if not __parent:
		printerr("No parent!")
		return ERR_CANT_ACQUIRE_RESOURCE
	__grandparent = __parent.get_parent()
	if not __grandparent:
		printerr("No grandparent!")
		return ERR_CANT_ACQUIRE_RESOURCE
	return OK


func has_shepherd():
	return null != __shepherd


func try_following_shepherd():
	if has_shepherd():
		if __shepherd.is_inside_tree():
			assert(__shepherd.position != Vector2.ZERO)
			set_position(__shepherd.get_position())
		else:
			stop_following_shepherd()


func start_following(shepherd:Node2D):
	__shepherd = shepherd


func stop_following_shepherd():
	__shepherd = null

