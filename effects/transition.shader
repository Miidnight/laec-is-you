shader_type canvas_item;

// A shadeer for transitions without backbuffer.
// Apply it for example to a ColorRect inside a CanvasLayer.

uniform bool is_playing = false;

uniform float start_time_ms = 0.0;  // ms, from OS.get_ticks_ms()
uniform float current_time_ms = 0.0;  // ms, from OS.get_ticks_ms()
uniform float duration_ms = 1000.0;  // ms

uniform vec4 fade_color : hint_color = vec4(0.0, 0.0, 0.0, 1.0);
uniform bool is_fade_out = false;

uniform bool is_fade = false;
uniform bool is_vertical_stripes = false;
uniform bool is_horizontal_stripes = false;
uniform bool is_checkerboard = false;
uniform bool is_dots = false;
uniform bool is_clock = false;


// Some options for the effects
uniform bool is_reversed = false;
uniform bool is_centered = false;
uniform bool is_flip_h = false;
uniform bool is_flip_v = false;

uniform int cell_size = 50;  // pixels
varying float f_cell_size;  // memoization, float of cell_size

const float SQRT_2 = 1.4142135623730951; // square root of 2
const float TAU = 6.283185307179586; // circle constant

// These are computed in the vertex(), for performance
varying float progress_time_ms; // ms
varying float progress; // 0 to 1
varying float progress_reverse; // 1 to 0 (memoization)


void vertex() {
	//float current_time_ms = TIME * 1000.0;
	
	// We need a call to max() because somehow, it starts negative
	progress_time_ms = max(0.0, current_time_ms - start_time_ms);
	progress = progress_time_ms / duration_ms;
	progress_reverse = 1.0 - progress;
	f_cell_size = float(cell_size);
}


void fragment() {
	if (is_playing) {
		if (progress >= 0.0 && progress <= 1.0) {
			
			// I. Prepare some variables
			
			vec2 fragment_coord = FRAGCOORD.xy;
			vec2 uv_coord = UV;
			if (is_flip_h) {
				uv_coord.x = 1.0 - uv_coord.x;
			}
			if (is_flip_v) {
				uv_coord.y = 1.0 - uv_coord.y;
			}
			
			vec3 final_color = fade_color.rgb;
			float opacity = 0.0;
			
			// II. Apply chosen effects
			
			if (is_fade) {
				opacity = fade_color.a * progress;
			}
			
			if (is_vertical_stripes || is_checkerboard) {
				float local = float(int(fragment_coord.x) % cell_size);
				float threshold = progress * f_cell_size;
				if (is_reversed) {
					local = f_cell_size - local;
				}
				if (is_centered) {
					local = abs(local - f_cell_size*0.5);
					threshold = progress * f_cell_size * 0.5;
				}
				if (local < threshold) {
					opacity = fade_color.a;
				}
			}
			
			if (is_horizontal_stripes || is_checkerboard) {
				float local = float(int(fragment_coord.y) % cell_size);
				float threshold = progress * f_cell_size;
				if (is_reversed) {
					local = f_cell_size - local;
				}
				if (is_centered) {
					local = abs(local - f_cell_size*0.5);
					threshold = progress * f_cell_size * 0.5;
				}
				if (local < threshold) {
					opacity = fade_color.a;
				}
			}
			
			if (is_dots) {
				vec2 local = vec2(
					float(int(fragment_coord.x) % cell_size),
					float(int(fragment_coord.y) % cell_size)
				);
				if (is_centered) {
					local = local - vec2(f_cell_size/2.0, f_cell_size/2.0);
				} else {
					local = local / 2.0;
				}
				if (
					length(local) * SQRT_2
					<=
					progress * float(cell_size)
				) {
					opacity = fade_color.a;
				}
			}
			
			if (is_clock) {
				vec2 local = vec2(
					uv_coord.x - 0.5,
					uv_coord.y - 0.5
				);
				
				float angle = atan(local.x, local.y) + TAU / 2.0;
				if (is_reversed) {
					angle = TAU - angle;
				}
				
				if (angle <= TAU * progress) {
					opacity = fade_color.a;
				}
			}
			
			if (is_fade_out) {
				opacity = 1.0 - opacity;
			}
			
			// III. Finally, set the color
			
			COLOR = vec4(final_color, opacity);
		} else {
			if (is_fade_out) {
				COLOR.a = 0.0;
			} else {
				COLOR = fade_color;
			}
		}
	} else {
		// The transition is not playing → let's be transparent
		COLOR.a = 0.0;
//		COLOR.r = 1.0;
	}
}