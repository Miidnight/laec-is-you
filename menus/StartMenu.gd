extends Control


func _ready():
	find_node("PlayButton").grab_focus()
	Jukebox.play()


func _input(_event):
	if Input.is_action_just_released("escape"):
		Game.go_back()


func _on_PlayButton_pressed():
	Game.switch_to_level("res://levels/PhiMapLevel.tscn")


func _on_ControlsButton_pressed():
	InputMapper.start_remapping([
		{
			'action': 'move_up',
			'title': tr('Up'),
		},
		{
			'action': 'move_down',
			'title': tr('Down'),
		},
		{
			'action': 'move_left',
			'title': tr('Left'),
		},
		{
			'action': 'move_right',
			'title': tr('Right'),
		},
		{
			'action': 'move_up_left',
			'title': tr('Up Left'),
		},
		{
			'action': 'move_down_left',
			'title': tr('Down Left'),
		},
		{
			'action': 'move_up_right',
			'title': tr('Up Right'),
		},
		{
			'action': 'move_down_right',
			'title': tr('Down Right'),
		},
	])


func _on_SettingsButton_pressed():
	Game.switch_to_level("res://menus/SettingsMenu.tscn")


func _on_ExitButton_pressed():
	App.exit()
