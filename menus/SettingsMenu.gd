extends Control


onready var music_slider = find_node('MusicVolumeHSlider')
onready var back_button = find_node('BackButton')
onready var fullscreen_check = find_node('FullscreenCheckButton')


func _ready():
	music_slider.value = App.get_setting('audio', 'main_volume')
	back_button.grab_focus()
	fullscreen_check.toggle_mode = App.is_fullscreen()


func _on_BackButton_pressed():
	Game.go_back_to_map()


func _on_MusicVolumeHSlider_value_changed(value):
	App.set_setting('audio', 'main_volume', value)
	App.adjust_main_volume_from_settings()


func _on_FullscreenCheckButton_pressed():
	App.toggle_fullscreen()

