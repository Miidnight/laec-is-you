extends Control


# Level this LevelVictory screen is about.
# Injected in prepare() by the factory.
var level  # Level scene instance
#var level_pickle : Dictionary


onready var continue_button = find_node("ContinueButton")
onready var more_button = find_node("MoreButton")
onready var title_label = find_node("LaecTitleLabel")
onready var excerpt_label = find_node("LaecExcerptLabel")


func _ready():
	self.continue_button.grab_focus()
	if self.level:
		# TODO: I18N with tr() ?
		self.title_label.text = self.level.title
	#	self.level_pickle = self.level.to_pickle()
	

func _on_ContinueButton_pressed():
	if not Game:
		printerr("`Game' singleton is not available.")
		return
	Game.go_back()


func prepare(for_level):
	assert(for_level, "Cannot prepare a victory screen for an empty level.")
	self.level = for_level
