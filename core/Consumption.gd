class_name Consumption


# This is not an abstraction of our society's behavior.
# This is used by the homebrewed (T_T) Sentence parser,
# in order to make backtracking easier.


var items_before := Array()
var items_consumed := Array()
var items_after := Array()


func _init(items:Array):
	# Not 100% certain we require duplication here,
	# but you know what they say about premature optimizations…
#	self.items_before = items
	self.items_before = items.duplicate()
	self.items_after = self.items_before


func has_happened() -> bool:
	return not self.items_consumed.empty()
