class_name Verb

var concept_name : String

func _to_string() -> String:  # for debugging purposes
	return "%s(%s)" % [
#		self.get_class(),  # == "Reference"
		'Verb',
		self.concept_name,
	]
