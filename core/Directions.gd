class_name Directions

# The 6 directions to neighbors in a pointy-topped regular hexagon lattice.

const LEFT = 'left'
const RIGHT = 'right'
const TOP_RIGHT = 'top_right'
const UP_RIGHT = TOP_RIGHT
const TOP_LEFT = 'top_left'
const UP_LEFT = TOP_LEFT
const BOTTOM_RIGHT = 'bottom_right'
const DOWN_RIGHT = BOTTOM_RIGHT
const BOTTOM_LEFT = 'bottom_left'
const DOWN_LEFT = BOTTOM_LEFT
const TOP = 'top'
const UP = TOP
const BOTTOM = 'bottom'
const DOWN = BOTTOM

const ALL = {  # fixme
	LEFT: Vector2(),
	RIGHT: Vector2(),
	TOP_LEFT: Vector2(),
	TOP_RIGHT: Vector2(),
	BOTTOM_RIGHT: Vector2(),
	BOTTOM_LEFT: Vector2(),
}

const ANGLE_STEP = TAU / 12.0

static func get_direction_from_vector(direction_vector:Vector2) -> String:
	var angle = direction_vector.angle()
	
	if (angle > -1 * ANGLE_STEP) and (angle < 1 * ANGLE_STEP):
		return RIGHT
	elif (angle > 1 * ANGLE_STEP) and (angle < 3 * ANGLE_STEP):
		return DOWN_RIGHT
	elif (angle > 3 * ANGLE_STEP) and (angle < 5 * ANGLE_STEP):
		return DOWN_LEFT
	elif (angle > 5 * ANGLE_STEP) or (angle < -5 * ANGLE_STEP):
		return LEFT
	elif (angle > -3 * ANGLE_STEP) and (angle < -1 * ANGLE_STEP):
		return UP_RIGHT
	elif (angle > -5 * ANGLE_STEP) and (angle < -3 * ANGLE_STEP):
		return UP_LEFT
	
	return RIGHT


static func get_direction_vector(direction:String) -> Vector2:
	if RIGHT == direction:
		return Vector2.RIGHT
	elif DOWN_RIGHT == direction:
		return Vector2(0.5, 0.866)
	elif DOWN == direction:
		return Vector2.DOWN
	elif DOWN_LEFT == direction:
		return Vector2(-0.5, 0.866)
	elif LEFT == direction:
		return Vector2.LEFT
	elif UP_LEFT == direction:
		return Vector2(-0.5, -0.866)
	elif UP == direction:
		return Vector2.UP
	elif UP_RIGHT == direction:
		return Vector2(0.5, -0.866)
	
	assert(false, "Unknown direction `%s'." % direction)
	return Vector2.ZERO


static func get_rotation(direction:String) -> float:
	if RIGHT == direction:
		return 0.0
	elif DOWN_RIGHT == direction:
		return ANGLE_STEP * 2.0
	elif DOWN == direction:
		return ANGLE_STEP * 3.0
	elif DOWN_LEFT == direction:
		return ANGLE_STEP * 4.0
	elif LEFT == direction:
		return ANGLE_STEP * 6.0
	elif UP_LEFT == direction:
		return ANGLE_STEP * 8.0
	elif UP == direction:
		return ANGLE_STEP * 9.0
	elif UP_RIGHT == direction:
		return ANGLE_STEP * 10.0
	
	assert(false, "Unknown direction `%s'." % direction)
	return 0.0

