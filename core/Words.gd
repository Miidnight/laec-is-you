class_name Words

# Essentially a database of the terminal symbols of our grammar.
# Except for the _things_, to make adding new things easy.
# Could also be a Resource, in the usual Godot fashion,
# but it might be a little less convenient, so this is fine.
# We're not going to add new words in here often, if at all.
# …
# Hello, curious reader !   I wish you a good day !  ;)


const UNDEFINED = 'undefined'


const VERB_IS = 'is'
const VERB_HAS = 'has'

const VERBS = [
	VERB_IS,
	VERB_HAS,
]


const OPERATOR_AND = 'and'
const OPERATOR_NOT = 'not'

const OPERATORS = VERBS + [  # perhaps don't include VERBS here?  TBD
	OPERATOR_AND,
	OPERATOR_NOT,
]


const QUALITY_YOU = 'you'
const QUALITY_WIN = 'win'
const QUALITY_DEFEAT = 'defeat'
const QUALITY_STOP = 'stop'
const QUALITY_PUSH = 'push'
const QUALITY_SINK = 'sink'
const QUALITY_WEAK = 'weak'
const QUALITY_OPEN = 'open'
const QUALITY_CLOSE = 'close'
const QUALITY_HOT = 'hot'
const QUALITY_MELT = 'melt'
# …

const QUALITIES = [
	QUALITY_YOU,
	QUALITY_WIN,
	QUALITY_DEFEAT,
	QUALITY_STOP,
	QUALITY_PUSH,
	QUALITY_SINK,
	QUALITY_WEAK,
	QUALITY_OPEN,
	QUALITY_CLOSE,
	QUALITY_HOT,
	QUALITY_MELT,
	# …
]


static func is_verb(concept:String) -> bool:
	return VERBS.has(concept)

static func is_operator(concept:String) -> bool:
	return OPERATORS.has(concept)

static func is_quality(concept:String) -> bool:
	return QUALITIES.has(concept)

static func is_thing(concept:String) -> bool:
	return not (
		is_verb(concept)
		or
		is_operator(concept)
		or
		is_quality(concept)
	)
