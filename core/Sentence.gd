class_name Sentence

# A Sentence of the Game Rules.
#
# Examples:
#
# - LAEC IS YOU
# - VOTE IS PUSH AND OPEN
#   → VOTE IS PUSH
#   → VOTE IS OPEN
# - URN AND DOOR IS CLOSE
#   → URN IS CLOSE
#   → DOOR IS CLOSE
# - NOT PHI AND TV IS WEAK
#   → (NOT PHI) IS WEAK
#   → TV IS WEAK
# - FRANCE HAS PHI
#
#
# Known Limitations:
# - Only one verb is allowed
# 	- FRANCE IS YOU AND HAS PHI
#   	→ FRANCE IS YOU
#
#
# See also `tests/Sentence.test.gd`.

const Item = preload("res://addons/laec-is-you/entity/Item.gd")


# We use this class to check the sentence's validity.
# Build a Sentence from a list of Items, and check this property afterwards,
# to see if the sentence makes any sense grammatically.
# If this is false, the other properties should be ignored,
# as they may not be set, and if they are they will be meaningless.
var is_valid := false

var subjects : Array  # of Subject
var verb : Verb
var complements : Array  # of Complement

# The list of Items composing this sentence
# This is useful to lit up the words and remove "embedded" sentences,
# like NOT BABA IS YOU not also yielding BABA IS YOU.
var items_used : Array  # of Item

################################################################################

func _to_string():
	return "Sentence(%s %s %s)" % [
		self.subjects,
		self.verb,
		self.complements,
	]

################################################################################

func are_items_text(items:Array) -> bool:
	for item in items:
		if not item.is_text:
			return false
	return true

################################################################################

# Hand-crafted parser, since we don't have lexer/parser generation from ANTLR,
# not any gdscript bindings for liboost.  (would they work in HTML exports?)
# Not a static factory, but could be.  (requires shenanigans, so this is fine)
# This is not meant to be run more than once.  (would need a reset() method)
func from_items(items:Array):
	assert(are_items_text(items))  # dev paranoia
	if not are_items_text(items):
		return

	var items_amount = items.size()
	if 3 > items_amount:
		return

	var items_left = items


	# I. Subject(s)

	var has_consumed_at_least_one_subject = false
	var first_subject = true
	var keep_finding_subjects = true

	while keep_finding_subjects:

		var and_consumption = Consumption.new(items_left)
		if not first_subject:
			and_consumption = consume_and(items_left)
			if and_consumption.has_happened():
				items_left = and_consumption.items_after
				self.items_used += and_consumption.items_consumed
			else:
				break

		var subject_consumption = consume_subject(items_left)

		if subject_consumption.has_happened():
			has_consumed_at_least_one_subject = true
			items_left = subject_consumption.items_after
			self.items_used += subject_consumption.items_consumed
		else:
			keep_finding_subjects = false

		first_subject = false

	if not has_consumed_at_least_one_subject:
		# We could not find any subject to consume (like Manuel Valls) → GTFO
		return


	# II. Verb

	var verb_consumption = consume_verb(items_left)
	var verb_concept = Words.UNDEFINED

	if not verb_consumption.has_happened():
		# We could not find any verb to consume → GTFO
		return
	else:
		verb_concept = verb_consumption.items_consumed[0].concept_name
		items_left = verb_consumption.items_after
		self.items_used += verb_consumption.items_consumed


	# III. Complement(s)
	
	var complements_filter = {'is_thing': true}
	if Words.VERB_IS == verb_concept:
		complements_filter = {}

	var has_consumed_at_least_one_complement = false
	var keep_finding_complements = true
	var first_complement = true

	while keep_finding_complements:

		var and_consumption = Consumption.new(items_left)
		if not first_complement:
			and_consumption = consume_and(items_left)
			if and_consumption.has_happened():
				items_left = and_consumption.items_after
				self.items_used += and_consumption.items_consumed
			else:
				break
		
		var complement_consumption = consume_complement(
			items_left, complements_filter
		)

		if complement_consumption.has_happened():
			has_consumed_at_least_one_complement = true
			items_left = complement_consumption.items_after
			self.items_used += complement_consumption.items_consumed
		else:
			keep_finding_complements = false
		
		first_complement = false

	if not has_consumed_at_least_one_complement:
		# We could not find any complement to consume → GTFO
		return

	# Everything checks out, it seems — let's tag this sentence as valid
	self.is_valid = true


################################################################################

func consume_subject(items:Array) -> Consumption:
	var consumption = Consumption.new(items)
	if not items:
		return consumption

	var subject = Subject.new()

	var prefix_consumption = consume_subject_prefix(
		items,
		subject
	)
	var body_consumption := consume_subject_body(
		prefix_consumption.items_after,
		subject
	)
#	var suffix_consumption = consume_subject_suffix(
#		body_consumption.items_after,
#		subject
#	)

	if body_consumption.has_happened():
		self.subjects.append(subject)
		consumption.items_consumed = (
			prefix_consumption.items_consumed
			+
			body_consumption.items_consumed
		)
		consumption.items_after = body_consumption.items_after

	return consumption


func consume_subject_prefix(items:Array, subject:Subject) -> Consumption:
	var consumption = Consumption.new(items)
	if not items:
		return consumption
	# TODO: This is where we'd consume LONELY
	return consumption


func consume_subject_body(items:Array, subject:Subject) -> Consumption:
	var consumption = Consumption.new(items)
	if not items:
		return consumption

	var negated = false
	var start := 0
	var amount_of_items = items.size()
	while items[start].is_operator_not():
		negated = not negated
		start += 1
		if start >= amount_of_items:
			# Bunch of NOTs at the end of the sentence → skip
			return consumption

	if items[start].is_thing:
		subject.negated = negated
		subject.concept_name = items[start].get_concept_name()

		consumption.items_consumed = items.slice(0, start)
		if start < items.size() - 1:
			consumption.items_after = items.slice(start + 1, items.size() - 1)

	return consumption


func consume_and(items:Array) -> Consumption:
	var consumption = Consumption.new(items)
	if not items:
		return consumption

	var item : Item = items[0]
	var item_concept = item.get_concept_name()
	if 'and' == item_concept:
		consumption.items_consumed.append(item)
		if 1 < items.size():
			consumption.items_after = items.slice(1, items.size() - 1)

	return consumption


func consume_verb(items:Array) -> Consumption:
	var consumption = Consumption.new(items)
	if not items:
		return consumption

	var item : Item = items[0]
	var item_concept = item.get_concept_name()
	if Words.VERBS.has(item_concept):
		var found_verb = Verb.new()
		found_verb.concept_name = item_concept
		self.verb = found_verb
		
		consumption.items_consumed.append(item)
		if 1 < items.size():
			consumption.items_after = items.slice(1, items.size() - 1)

	return consumption


func consume_complement(items:Array, filters:Dictionary) -> Consumption:
	var consumption = Consumption.new(items)
	if not items:
		return consumption
	
	var complement = Complement.new()

	var body_consumption := consume_complement_body(
		items,
		complement,
		filters
	)
	
	if body_consumption.has_happened():
		self.complements.append(complement)
		consumption.items_consumed = (
#			prefix_consumption.items_consumed
#			+
			body_consumption.items_consumed
		)
		consumption.items_after = body_consumption.items_after
	
	return consumption


func consume_complement_body(items:Array, complement:Complement, filters:Dictionary) -> Consumption:
	var consumption = Consumption.new(items)
	if not items:
		return consumption
	
	var negated = false
	var start := 0
	var amount_of_items = items.size()
	while items[start].is_operator_not():
		negated = not negated
		start += 1
		if start >= amount_of_items:
			return consumption

	var item : Item = items[start]
	if (
		(item.is_thing or item.is_quality)
		and
		does_item_match_filters(item, filters)
	):
		complement.negated = negated
		complement.concept_name = items[start].get_concept_name()
		
		consumption.items_consumed = items.slice(0, start)
		if start < items.size() - 1:
			consumption.items_after = items.slice(start + 1, items.size() - 1)

	return consumption


func does_item_match_filters(item:Item, filters:Dictionary) -> bool:
	for filter_property in filters:
		if item.get(filter_property) != filters[filter_property]:
			return false
	return true


################################################################################

func contains_sentence(other_sentence:Sentence) -> bool:
	for item in other_sentence.items_used:
		if not self.items_used.has(item):
			return false
	return true

################################################################################

# BABA AND CRAB IS YOU
# BABA IS YOU AND CRAB
# BABA IS YOU AND HAS NOT CRAB
# → BABA IS YOU
# → BABA HAS NOT CRAB
# BABA IS YOU AND NOT CRAB
# BABA NOT CRAB IS YOU => NOT CRAB IS YOU

#func from_string(sentence_string:String):
#	var sentence_regex = RegEx.new()
#	sentence_regex.compile(
#		"^" +
#		"(?<prefix_full>(?<prefix_negation>(?:not )+|)(?<prefix>lonely) |)" +
#		"(?<things>" +
#			"(?<thing_negation>(?:not )+|)(?<thing>[a-z]+)" +
#		")" +
#		" +" +
#		"(?<verb>%s)" % [array_join(VERBS, '|')] +
#		" +" +
#		"(?<complements>.+)" +
##		"(?<complements>(?:" +
##			"(?: and )?" +
##			"(?<quality>[a-z]+)" +
##		")+)" +
#		"$"
#	)
#
#	var result = sentence_regex.search(sentence_string)
#	if not result:
#		return
#
#	prints('prefix_full', result.get_string('prefix_full'))
#	prints('prefix_negation', result.get_string('prefix_negation'))
#	prints('prefix', result.get_string('prefix'))
#	prints('thing_negation', result.get_string('thing_negation'))
#	prints('thing', result.get_string('thing'))
#	prints('verb', result.get_string('verb'))
#	prints('qualities', result.get_string('qualities'))
#	prints('quality', result.get_string('quality'))
#	prints('complements', result.get_string('complements'))
#
#	print(result.strings)
#
##	var complements_regex = RegEx.new()
##	complements_regex.compile(
##		"^(?:([a-z]+)(?: and )?)+$"
##	)
##	var complements_result = complements_regex.search(result.get_string('complements'))
##	if not complements_result:
##		return
#
#	self.complements = result.get_string('complements').split(' and ')
#	if not complements:
#		return
#
##	for i in range(self.complements)
#
##	print(complements_result.strings)
##	self.complements = complements_result.strings
##	self.complements.pop_front()
#
#	self.is_valid = true
#	self.verb = result.get_string('verb')
#	self.subjects_prefix = result.get_string('prefix')
#
#
#
#func array_join(arr : Array, glue : String = '') -> String:
#	var string : String = ''
#	for index in range(0, arr.size()):
#		string += str(arr[index])
#		if index < arr.size() - 1:
#			string += glue
#	return string
