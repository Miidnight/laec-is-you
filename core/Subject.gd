class_name Subject

# One Subject of a Sentence.
# A Sentence may have multiple Subjects

var concept_name : String = ''  # eg: baba
var negated : bool = false

var prefix : String = ''  # eg: lonely
var prefix_negated : bool = false

var is_thing : bool setget ,get_is_thing

func _to_string() -> String:  # for debugging purposes
	return "%s(%s%s%s%s)" % [
#		self.get_class(),  # == "Reference"
		'Subject',
		'not ' if self.prefix_negated else '',
		self.prefix + (' ' if self.prefix else ''),
		'not ' if self.negated else '',
		self.concept_name,
	]

func get_is_thing() -> bool:
	return Words.is_thing(self.concept_name)
