extends Node

#  _____                   _   __  __
# |_   _|                 | | |  \/  |
#   | |  _ __  _ __  _   _| |_| \  / | __ _ _ __  _ __   ___ _ __
#   | | | '_ \| '_ \| | | | __| |\/| |/ _` | '_ \| '_ \ / _ \ '__|
#  _| |_| | | | |_) | |_| | |_| |  | | (_| | |_) | |_) |  __/ |
# |_____|_| |_| .__/ \__,_|\__|_|  |_|\__,_| .__/| .__/ \___|_|
#             | |                          | |   | |
#             |_|                          |_|   |_|
# 
# A Singleton to help deal with custom input mappings.
# 
# Usage Example
# -------------
# 
# InputMapper.start_remapping([
# 	{ 'action': 'move_left', 'title': tr('Move Left') },
# 	{ 'action': 'move_right', 'title': tr('Move Right') },
# ])


var custom_mapping_file = "user://custom_input_mapping.dat"

var current_scene : Node
var container : Container
var vbox : Container
var action_label : Label
var action_mapping : Label

var is_remapping = false
var actions_to_remap : Array
var action_to_remap_index : int

var custom_mapping : Array  # of Dictionary with `action` and `event`


func _ready():
	#print("InputMapper is readying…")
	__load_custom_mapping()
	#print("InputMapper is ready.")


func _input(event):
	if not is_remapping:
		return
	if not (event is InputEventKey):
		return
	if not event.pressed:
		return
	
	var action_to_remap = actions_to_remap[action_to_remap_index]
	__remap(action_to_remap['action'], event)
	action_mapping.text = OS.get_scancode_string(event.scancode)
	custom_mapping.append({
		'action': action_to_remap['action'],
		'event': event,
	})
	__move_to_next_action()


################################################################################


func start_remapping(actions:Array):
	current_scene = get_tree().get_current_scene()
	current_scene.visible = false
	
	assert(
		not actions.empty(),
		"Provide an array of Dictionaries with keys `title` and `action`."
	)
	
	is_remapping = true
	custom_mapping = Array()
	actions_to_remap = actions
	action_to_remap_index = -1
	
	container = CenterContainer.new()
	container.anchor_left = 0.0
	container.anchor_top = 0.0
	container.anchor_right = 1.0
	container.anchor_bottom = 1.0
	
	vbox = VBoxContainer.new()
	container.add_child(vbox)
	add_child(container)
	
	InputMap.load_from_globals()  # bit rough, but works
	__move_to_next_action()


################################################################################


func __load_custom_mapping():
	var file := File.new()
	if not file.file_exists(custom_mapping_file):
		custom_mapping = Array()
		return
	var err = file.open(custom_mapping_file, File.READ)
	if err != OK:
		printerr("Failed to load input mapping `%s`." % custom_mapping_file)
		return
	
	custom_mapping = str2var(file.get_as_text())
	for mapping in custom_mapping:
		__remap(mapping['action'], mapping['event'])
	#print("Loaded custom mapping from file `%s`." % custom_mapping_file)


func __save_custom_mapping():
	var file := File.new()
	var err = file.open(custom_mapping_file, File.WRITE)
	if err != OK:
		printerr("Failed to save input mapping `%s`." % custom_mapping_file)
		return
	file.store_string(var2str(custom_mapping))
	#print("Saved custom mapping to file `%s`." % custom_mapping_file)


func __move_to_next_action():
	action_to_remap_index += 1
	if action_to_remap_index >= actions_to_remap.size():
		is_remapping = false
		__save_custom_mapping()
		yield(get_tree().create_timer(1.618), "timeout")
		container.get_parent().remove_child(container)
		current_scene.visible = true
		return
	var action_to_remap = actions_to_remap[action_to_remap_index]
	
	var hbox = HBoxContainer.new()
	action_label = Label.new()
	action_label.rect_min_size = Vector2(150, 0)
#	action_label.align = action_label.ALIGN_RIGHT
	action_mapping = Label.new()
	action_mapping.rect_min_size = Vector2(150, 0)
	hbox.add_child(action_label)
	hbox.add_child(action_mapping)
	vbox.add_child(hbox)
	action_label.text = action_to_remap['title']
	action_mapping.text = "<"+tr('press key')+">"


func __remap(action_to_remap, event):
	#print("Remapping `%s'…" % [action_to_remap])
	for action in InputMap.get_actions():
		if InputMap.action_has_event(action, event):
			#print("Overriding action `%s`." % action)
			InputMap.action_erase_event(action, event)
	InputMap.action_add_event(action_to_remap, event)
