tool
extends "res://addons/laec-is-you/entity/Item.gd"


# 
# Partals help navigate between levels.
# 


export(String, FILE, "*.tscn") var level_path


# Perhaps restore this later,
# at least provide _some_ way of customizing the portals,
# so that we can hide invisible portals in the levels.
#export(Texture) var texture
#export(Texture) var texture_when_completed


func _input(event):
	if (
		(event is InputEventKey or event is InputEventJoypadButton)
		and event.pressed
		and (
			Input.is_action_just_pressed("ui_accept")
			or
			Input.is_action_just_pressed("pass")
			or
			Input.is_action_just_pressed("enter")
		)
	):
		perhaps_open()


func perhaps_open() -> bool:
	if not is_piled_with_you():
		return false
	
	print("Opening Portal `%s'…" % self.name)
	if not Game:
		printerr("Singleton `Game' is not defined.")
		return false
	Game.enter_level(self.level_path)
	return true


func rename():
	pass  # leave the portals' names alone


func update_sprite():
	var concept = 'portal'
	if Engine.editor_hint:
		concept = 'portal'
	elif Game and Game.is_level_complete(level_path):
		concept = 'portal_completed'
	# We *could* build only one of these (well, two) for all the doors
	var sf = SpriteFramesFactory.get_for_concept(concept, false, 'portals')
	$AnimatedSprite.frames = sf
	$AnimatedSprite.animation = get_animation_name()


func is_piled_with_you():
	var it_is = false
	for item in get_level().get_all_items():
		if is_piled_with(item) and item.is_you:
			it_is = true
			break
	return it_is


func is_piled_with(other_item):
	return self.cell_position == other_item.cell_position
