tool
extends Node2D
#class_name Item  # tempting, but no

# Items are the main entities in the game.
# 
# It handles:
# - its flags, that the level will set when spending a turn.
# - its appearance, from its flags
# 
# Make sure there are images `<thing>.png` and `<thing>-text.png`
# in `sprites/items`.
# 
# More properties (for animated textures, directional textures, …)
# may be added later through Resources.  (TBD)
# 

# This is relevant only for things.
# Always set this to true for operators and qualities.
export(bool) var is_text = false

# Describe the concept of this item.
# Only one of these may be set.
# If somehow multiple are provided, behavior is undefined.
# Right now any keyword can be used
# as long as there are icons for it.
# - sprites/items/<name>.png
# - sprites/items/<name>-text.png
# Please only use a single word, lowercase, no diacritics.
export(String) var concept_name = ''
#export(String) var thing_name
#export(String) var operator_name
#export(String) var quality_name
# In the future we may use setget to update parts of the Editor,
# and also set is_text, and make sure only one is set, etc.
#export(String) var thing_name setget set_thing_name, get_thing_name
#export(String) var operator_name setget set_operator_name, get_operator_name
#export(String) var quality_name setget set_quality_name, get_quality_name

# Emitted when one of the above properties change,
#signal concept_changed

# Read-only concept flags.
# In this class, use the fully qualified self.is_xxx
# instead of the shortcut is_xxx, to trigger setgets.
var is_thing setget set_is_thing, get_is_thing
var is_operator setget set_is_operator, get_is_operator
var is_quality setget set_is_quality, get_is_quality

var latticeability := true

# This is exported for the ItemWizard.
# It derives from the node's xy position.
# The game uses this property,
# as the level's lattice reindexes itself from it.
# It is deprecated, use cell_position instead
export(Vector2) var hexagon_position := Vector2.ZERO
# Forward compat for other lattices than hexagons, use instead of hexagon_position
export(Vector2) var cell_position := Vector2.ZERO setget set_cell_position, get_cell_position

# The direction this Item is facing.
# The direction is changed when Items move or are moved.
export(String) var direction = Directions.RIGHT

export(int) var animation_state = 0


# State | Qualities flags.
# These are exported for the Item Wizard.
# Best not edit those yourself.
# They will be reset by the sentences on each turn.
export(bool) var is_you = false
export(bool) var is_win = false
export(bool) var is_stop = false
export(bool) var is_push = false
export(bool) var is_defeat = false
export(bool) var is_sink = false
export(bool) var is_open = false
export(bool) var is_shut = false
export(bool) var is_weak = false
export(bool) var is_hot = false
export(bool) var is_melt = false

export(bool) var is_lit = true setget set_lit
export(bool) var is_ignored = false setget set_ignored


# When this Item is destroyed, it will release its stuffing (HAS verb)
var stuffing = Array()  # of things' concept names (String)


onready var ignored_animation = $IgnoredAnimatedSprite


#   _______     __                                   _ _          _   _
#  / /  __ \    \ \                                 | (_)        | | (_)
# | || |  | | ___| |_ __   ___  _ __ _ __ ___   __ _| |_ ______ _| |_ _  ___  _ __
# | || |  | |/ _ \ | '_ \ / _ \| '__| '_ ` _ \ / _` | | |_  / _` | __| |/ _ \| '_ \
# | || |__| |  __/ | | | | (_) | |  | | | | | | (_| | | |/ / (_| | |_| | (_) | | | |
# | ||_____/ \___| |_| |_|\___/|_|  |_| |_| |_|\__,_|_|_/___\__,_|\__|_|\___/|_| |_|
#  \_\          /_/
#

func reset_properties():
	self.is_you = false
	self.is_win = false
	self.is_stop = false
	self.is_push = false
	self.is_defeat = false
	self.is_sink = false
	self.is_open = false
	self.is_shut = false
	self.is_weak = false
	self.is_hot = false
	self.is_melt = false
	
	if self.is_text:
		self.is_lit = false
	else:
		self.is_lit = true
	self.is_ignored = false
	
	self.stuffing = Array()

func to_pickle():
	return {
		'is_you': self.is_you,
		'is_win': self.is_win,
		'is_stop': self.is_stop,
		'is_push': self.is_push,
		'is_defeat': self.is_defeat,
		'is_sink': self.is_sink,
		'is_open': self.is_open,
		'is_shut': self.is_shut,
		'is_weak': self.is_weak,
		'is_hot': self.is_hot,
		'is_melt': self.is_melt,
		
		'is_lit': self.is_lit,
		'is_text': self.is_text,
		
		'stuffing': self.stuffing,
		
		'concept_name': self.concept_name,
		
#		'hexagon_position': self.hexagon_position,
		'cell_position': self.cell_position,
	}

func from_pickle(rick):
	for property in rick.keys():
		#prints("[%s] set"%name, property, rick[property])
		self.set(property, rick[property])


#   _____      _
#  / ____|    | |
# | (___   ___| |_ _   _ _ __
#  \___ \ / _ \ __| | | | '_ \
#  ____) |  __/ |_| |_| | |_) |
# |_____/ \___|\__|\__,_| .__/
#                       | |
#                       |_|

func _ready():
	ready()

func ready():
	if Engine.editor_hint:
		snap_to_grid()
	reposition()
	get_sprite().frame = randi() % 3
	update_sprite()
	update_particles()
	# Do NOT rename the node here, or the Editor will not select
	# the appropriate node after duplication in the scene tree.
	#rename()


#   _____
#  / ____|                         /\
# | (___   ___ ___ _ __   ___     /  \__      ____ _ _ __ ___ _ __   ___  ___ ___
#  \___ \ / __/ _ \ '_ \ / _ \   / /\ \ \ /\ / / _` | '__/ _ \ '_ \ / _ \/ __/ __|
#  ____) | (_|  __/ | | |  __/  / ____ \ V  V / (_| | | |  __/ | | |  __/\__ \__ \
# |_____/ \___\___|_| |_|\___| /_/    \_\_/\_/ \__,_|_|  \___|_| |_|\___||___/___/
#
#

func get_scene_viewport():
	var parent = self
	while parent and not (parent is Viewport):
		parent = parent.get_parent()
	assert(parent is Viewport, "Scene viewport was not found.")
	return parent

func get_cells_map():
	var cell_map = get_scene_viewport().find_node('HexagonalTileMap', true, false)
	return cell_map

func get_level():
	var LevelType = load("res://addons/laec-is-you/entity/Level.gd")  # ~cyclic
	for node in get_scene_viewport().get_children():
		if node is LevelType:
			return node
	assert(false, "Level was not found in the scene.")


#          _   _        _ _           _
#     /\  | | | |      (_) |         | |
#    /  \ | |_| |_ _ __ _| |__  _   _| |_ ___  ___
#   / /\ \| __| __| '__| | '_ \| | | | __/ _ \/ __|
#  / ____ \ |_| |_| |  | | |_) | |_| | ||  __/\__ \
# /_/    \_\__|\__|_|  |_|_.__/ \__,_|\__\___||___/
#
#

func show_read_only_notice(concept_name, concept_example, value):
	printerr(
		"Don't set is_%s to %s. Consider it read-only.\n" % [
			concept_name, value
		] +
		"Instead, set %s_name to a String, like '%s'." % [
			concept_name, concept_example
		]
	)

func set_is_thing(value):
	show_read_only_notice('thing', 'baba', value)

func get_is_thing():
	if not concept_name:
		return false
	return not (
		Words.OPERATORS.has(concept_name)
		or
		Words.QUALITIES.has(concept_name)
	)

func set_is_operator(value):
	show_read_only_notice('operator', 'is', value)

func get_is_operator():
	return Words.OPERATORS.has(concept_name)

func set_is_quality(value):
	show_read_only_notice('quality', 'you', value)

func get_is_quality():
	return Words.QUALITIES.has(concept_name)

func set_lit(lit):
	is_lit = lit
	update_salience(lit)

func set_ignored(ignored):
	is_ignored = ignored
	if self.ignored_animation:
		self.ignored_animation.visible = is_ignored

func is_operator_not() -> bool:
	return (
		self.is_text
		and
		# We could check operator_name, but since we might remove the trinity
		# and only use concept_name for simplicity in the future,
		# let's be forward-compatible from the get-go.
		get_concept_name() == 'not'
	)


#  _   _
# | \ | |
# |  \| | __ _ _ __ ___   ___
# | . ` |/ _` | '_ ` _ \ / _ \
# | |\  | (_| | | | | | |  __/
# |_| \_|\__,_|_| |_| |_|\___|
#
#

func rename():
	self.name = get_concept_name().capitalize() + ('Text' if self.is_text else '')

func get_concept_name():
	if self.concept_name:
		return self.concept_name
	return 'undefined'  # here be trolls


#           _      _
#     /\   | |    | |
#    /  \  | | ___| |__   ___ _ __ ___  _   _
#   / /\ \ | |/ __| '_ \ / _ \ '_ ` _ \| | | |
#  / ____ \| | (__| | | |  __/ | | | | | |_| |
# /_/    \_\_|\___|_| |_|\___|_| |_| |_|\__, |
#                                        __/ |
#                                       |___/

func qualify(quality_name):
	var n = "is_%s" % quality_name
	assert(n in self, "The quality `%s` is undefined in Item." % [quality_name])
	if n in self:
		set(n, true)

func transmute(thing_name):
	self.concept_name = thing_name
	update_sprite()
	rename()

func stuff_with(thing_name):
	self.stuffing.append(thing_name)


#
#     /\
#    /  \   _ __  _ __   ___  __ _ _ __ __ _ _ __   ___ ___
#   / /\ \ | '_ \| '_ \ / _ \/ _` | '__/ _` | '_ \ / __/ _ \
#  / ____ \| |_) | |_) |  __/ (_| | | | (_| | | | | (_|  __/
# /_/    \_\ .__/| .__/ \___|\__,_|_|  \__,_|_| |_|\___\___|
#          | |   | |
#          |_|   |_|

func get_sprite():
	return $AnimatedSprite

func update_aesthetics():
	update_sprite()
	update_salience()
	update_particles()

func update_sprite():
	var concept = get_concept_name()
	var sf = SpriteFramesFactory.get_for_concept(concept, self.is_text)
	$AnimatedSprite.frames = sf
	$AnimatedSprite.animation = get_animation_name()

func update_particles():
	$WinParticles.emitting = self.is_win

func update_salience(is_increase=null):
	if null == is_increase:
		is_increase = is_lit
	if is_increase:
		self.modulate = Color(1.0, 1.0, 1.0, 1.0)
	else:
		var darker = 0.9
		self.modulate = Color(darker, darker, darker, 0.7)

func raise_dust():
	yield(get_tree().create_timer(0.08), "timeout")
	# We use many emitters because emission happens faster
	# than the emitters' lifecycle, and it's faster to do this
	# than to make our own particle emitter.
	# Refactor at will (especially for water movement customization!)
	var emittor = $MoveParticles1
	if emittor.emitting:
		emittor = $MoveParticles2
	if emittor.emitting:
		emittor = $MoveParticles3
	if emittor.emitting:
		emittor = $MoveParticles4
	if emittor.emitting:
		emittor = $MoveParticles5
	if emittor.emitting:
		emittor = $MoveParticles6
	emittor.direction = -1 * Directions.get_direction_vector(self.direction)
	emittor.emitting = true
	emittor.restart()

func animate_destruction():
	$DestructionParticles.restart()

func get_animation_name():
	return "%s_%d" % [
		self.direction, self.animation_state
	]

func advance_animation():
	self.animation_state = (self.animation_state + 1)
	if not get_sprite().frames.has_animation(get_animation_name()):
		self.animation_state = 0


#   _____
#  / ____|
# | (___  _ __   __ _  ___ ___
#  \___ \| '_ \ / _` |/ __/ _ \
#  ____) | |_) | (_| | (_|  __/
# |_____/| .__/ \__,_|\___\___|
#        | |
#        |_|

func is_latticeable() -> bool:
	return self.latticeability

func set_latticeability(latticeability:bool):
	self.latticeability = latticeability

func set_cell_position(new_cell_position:Vector2) -> void:
	hexagon_position = new_cell_position

func get_cell_position() -> Vector2:
	return hexagon_position

func infer_position():
	snap_to_grid()
	reposition()

func set_position(new_position:Vector2):
	# This crutch allows us to use Ysort.
	# Z-indices don't work well with inherited scenes in Dialogs.
	return .set_position(
		new_position
		+
		Vector2(0, 0.0001 * get_z_salience())
	)

func reposition(teleport=false):
	var cell_map = get_cells_map()
	if not cell_map:
		printerr("No Cell Map found, skipping reposition()…")
		return
	var target_position = cell_map.map_to_world(self.cell_position)
	if teleport:
		self.position = target_position
	else:
		$Tween.interpolate_property(
			self,  # object
			'position',  # property
			self.position,  # initial
			target_position,  # target
			0.155,  # duration (should almost be `action cooldown`)
			Tween.TRANS_LINEAR,  # transition
			Tween.EASE_IN_OUT,  # easing
			0  # delay
		)
		$Tween.start()

func snap_to_grid():
	var cell_map = get_cells_map()
	if not cell_map:
		printerr("No Cell Map found, skipping snap_to_grid()…")
		return
	self.cell_position = cell_map.world_to_map(self.position)
	reposition()


func is_looking_left() -> bool:
	# Since We're using pointy-topped hexagons,
	# an item is either looking left or right.
	return not is_looking_right()


func is_looking_right() -> bool:
	return (
		self.direction == Directions.RIGHT
		or
		self.direction == Directions.TOP_RIGHT
		or
		self.direction == Directions.BOTTOM_RIGHT
	)


const SALIENCE_NONE := 0.0
const SALIENCE_SOME := 0.1
const SALIENCE_LOTS := 1.0
const SALIENCE_HUGE := 10.0


var saliences_of_qualities := {
	Words.QUALITY_YOU: SALIENCE_HUGE,
}

func get_z_salience() -> float:
	var z_salience = 0.0
	
	for quality in Words.QUALITIES:
		if get("is_%s" % quality):  # perhaps inefficient (reflection !?)
			z_salience += saliences_of_qualities.get(quality, SALIENCE_SOME)
	
	return z_salience
	
