tool
extends Node2D
#class_name Level  # (┛◉.◉)┛彡┻━┻

# A Level handles:
# - a bunch of items
# - a lattice of cells
# - a tilemap of ground tiles
# - applying the game rules
# - win and defeat animations
# - its own history (UNDO IS ODNU)
# - … ?
# 
# A handy blackboard for new features, until it's refactoring time.
# 
# A Level should be able to run in the Editor.
# Keep in mind that Singletons are not available in the Editor.
# Some features are explicitely disabled in the editor, such as:
# - Alchemical Sentences  (THING IS THING)
# - Stuffing Sentences  (THING HAS THING)

const Item = preload("res://addons/laec-is-you/entity/Item.gd")


export(String) var title = ""
export(String) var subtitle = ""


onready var items_layer : Node2D = $Items
onready var effects_layer : Node2D = $Effects


# A "dictionary" of Cell => Array of Items, with hex goodies
var cell_lattice : HexagonalLattice = HexagonalLattice.new()


# Helps with THING IS NOT THING transmutation.
# This is set at the beginning of the level, from initial items.
var __unique_things_available : Array


#  _      _  __                     _
# | |    (_)/ _|                   | |
# | |     _| |_ ___  ___ _   _  ___| | ___
# | |    | |  _/ _ \/ __| | | |/ __| |/ _ \
# | |____| | ||  __/ (__| |_| | (__| |  __/
# |______|_|_| \___|\___|\__, |\___|_|\___|
#                         __/ |
#                        |___/
# 
# Children classes may override ready() since they can't override _ready().
# The same goes for _process().


func _ready():
	ready()


func _process(delta):
	process(delta)


func ready():
	# This initial turn would handle the initial
	# setting of item flags from the rules.
	# But we don't want this.  Hence the ItemWizard.
	#spend_turn()
	
	# We need to register the Items, it's like a cache
	# so we don't look at the scene tree all the time. (it's super slow)
	register_all_items()

	# We want to write the initial state in history,
	# so we can get back to it by pressing undo.
	write_history()
	
	__unique_things_available = get_unique_things_available()
	
	enable_inputs_on_startup()


func process(_delta):
	handle_inputs()
	handle_mouse_inputs_on_process()


func go_back_to_map() -> void:
	go_back()


func go_back() -> void:
	if not Game:
		return
	Game.go_back()


#   _____
#  / ____|                         /\
# | (___   ___ ___ _ __   ___     /  \__      ____ _ _ __ ___ _ __   ___  ___ ___
#  \___ \ / __/ _ \ '_ \ / _ \   / /\ \ \ /\ / / _` | '__/ _ \ '_ \ / _ \/ __/ __|
#  ____) | (_|  __/ | | |  __/  / ____ \ V  V / (_| | | |  __/ | | |  __/\__ \__ \
# |_____/ \___\___|_| |_|\___| /_/    \_\_/\_/ \__,_|_|  \___|_| |_|\___||___/___/
#
#

func get_scene_viewport():
	var parent = self
	while parent and not (parent is Viewport):
		parent = parent.get_parent()
	assert(parent is Viewport, "Scene viewport was not found.")
	return parent


func get_cell_map():
	var hex_map = get_scene_viewport().find_node('HexagonalTileMap', true, false)
	assert(hex_map, 'HexagonalTileMap was not found in the scene.')
	return hex_map


#  _    _           _
# | |  | |         | |
# | |__| |_   _  __| |
# |  __  | | | |/ _` |
# | |  | | |_| | (_| |
# |_|  |_|\__,_|\__,_|
#
#

onready var mouse_gui : HexagonGui = $Hud/HexagonGui

var mouse_gui_fill_sensitivity = 0.025


func show_mouse_gui():
	mouse_gui.release_direction()
	mouse_gui.set_visible(true)


func hide_mouse_gui():
	mouse_gui.release_direction()
	mouse_gui.set_visible(false)


func move_mouse_gui_at_position(here:Vector2) -> void:
	mouse_gui.position = here


func mark_direction_in_mouse_gui(direction:String):
	mouse_gui.mark_direction(direction)


func release_direction_in_mouse_gui():
	mouse_gui.release_direction()


func fill_mouse_gui(how_much:float=1.0):
	mouse_gui.fill(how_much)


func empty_mouse_gui():
	release_direction_in_mouse_gui()
	fill_mouse_gui(0)


#  _____                   _
# |_   _|                 | |
#   | |  _ __  _ __  _   _| |_ ___
#   | | | '_ \| '_ \| | | | __/ __|
#  _| |_| | | | |_) | |_| | |_\__ \
# |_____|_| |_| .__/ \__,_|\__|___/
#             | |
#             |_|

var __is_accepting_inputs := false

var action_cooldown = 0.16   # seconds
var last_action_stamp = 0.0  # milliseconds


func _input(event):
	if not is_accepting_inputs():
		return
	
	if event is InputEventJoypadButton or event is InputEventKey:
		if Input.is_action_just_released("escape"):
			stop_accepting_inputs()
			go_back_to_map()
		if Input.is_action_just_released("restart"):
			stop_accepting_inputs()
			Game.reset_level()
		if Input.is_action_just_released("pass"):
			if not self.is_in_limbo and not is_cooling_down_between_actions():
				spend_turn()
				start_action_cooldown()
	
	if event is InputEventMouse:
		handle_mouse_inputs(event)


func start_accepting_inputs() -> void:
	self.__is_accepting_inputs = true
	# … signals


func stop_accepting_inputs() -> void:
	self.__is_accepting_inputs = false
	hide_mouse_gui()
	# … signals


func is_accepting_inputs() -> bool:
	return self.__is_accepting_inputs


func enable_inputs_on_startup():
	yield(get_tree().create_timer(0.8), "timeout")
	start_accepting_inputs()


func is_cooling_down_between_actions(now_ms:int=-1) -> bool:
	if 0 > now_ms:
		now_ms = OS.get_ticks_msec()
	if now_ms < self.last_action_stamp + get_action_cooldown_ms():
		return true
	return false


func start_action_cooldown(now_ms:int=-1) -> void:
	if 0 > now_ms:
		now_ms = OS.get_ticks_msec()
	self.last_action_stamp = now_ms % (1 << 30)


func get_action_cooldown():
	return action_cooldown


func get_action_cooldown_ms():
	return action_cooldown * 1000


func handle_inputs():
	if not is_accepting_inputs():
		return
	if is_cooling_down_between_actions():
		return

	var can_move = not self.is_in_limbo
	var has_moved = false
	var has_tried_to_move = false
	
	if can_move:
		if (
			(not has_moved)
			and
			Input.is_action_pressed("move_right")
			and
			Input.is_action_pressed("move_up")
		):
			has_tried_to_move = true
			has_moved = try_move(Directions.UP_RIGHT)
		if (
			(not has_moved)
			and
			Input.is_action_pressed("move_left")
			and
			Input.is_action_pressed("move_up")
		):
			has_tried_to_move = true
			has_moved = try_move(Directions.UP_LEFT)
		if (
			(not has_moved)
			and
			Input.is_action_pressed("move_right")
			and
			Input.is_action_pressed("move_down")
		):
			has_tried_to_move = true
			has_moved = try_move(Directions.DOWN_RIGHT)
		if (
			(not has_moved)
			and
			Input.is_action_pressed("move_left")
			and
			Input.is_action_pressed("move_down")
		):
			has_tried_to_move = true
			has_moved = try_move(Directions.DOWN_LEFT)
		if (not has_moved) and Input.is_action_pressed("move_right"):
			has_tried_to_move = true
			has_moved = try_move(Directions.RIGHT)
		if (not has_moved) and Input.is_action_pressed("move_up_right"):
			has_tried_to_move = true
			has_moved = try_move(Directions.UP_RIGHT)
		if (not has_moved) and Input.is_action_pressed("move_up"):
			has_tried_to_move = true
			has_moved = try_move(Directions.UP)
		if (not has_moved) and Input.is_action_pressed("move_up_left"):
			has_tried_to_move = true
			has_moved = try_move(Directions.UP_LEFT)
		if (not has_moved) and Input.is_action_pressed("move_left"):
			has_tried_to_move = true
			has_moved = try_move(Directions.LEFT)
		if (not has_moved) and Input.is_action_pressed("move_down_left"):
			has_tried_to_move = true
			has_moved = try_move(Directions.DOWN_LEFT)
		if (not has_moved) and Input.is_action_pressed("move_down"):
			has_tried_to_move = true
			has_moved = try_move(Directions.DOWN)
		if (not has_moved) and Input.is_action_pressed("move_down_right"):
			has_tried_to_move = true
			has_moved = try_move(Directions.DOWN_RIGHT)

	if has_tried_to_move:
		start_action_cooldown()
		spend_turn()

	var has_undoed = false
	if (not has_moved) and Input.is_action_pressed("undo"):
		has_undoed = true
		undo()

	if has_moved or has_undoed:  # has_acted
		start_action_cooldown()


var __mouse_controls_deadzone := pow(33, 2)
var __is_currently_using_drag_joystick := false
var __mouse_controls_start_position := Vector2.ZERO
var __mouse_controls_pressure_gauge := 0.0

func handle_mouse_inputs(event:InputEventMouse) -> void:
	if (
		event is InputEventMouseButton
		and
		(
			BUTTON_LEFT == event.button_index
#			or
#			BUTTON_RIGHT == event.button_index
		)
	):
		if event.pressed:
			__is_currently_using_drag_joystick = true
			__mouse_controls_start_position = event.position
			show_mouse_gui()
			move_mouse_gui_at_position(get_global_mouse_position())
		else:
			__is_currently_using_drag_joystick = false
			__mouse_controls_start_position = Vector2.ZERO
			hide_mouse_gui()
#		prints('Using mouse controls: ', __is_currently_using_drag_joystick)
	

func handle_mouse_inputs_on_process() -> void:	
	if not is_accepting_inputs():
		return
	
	var mouse_position = get_tree().get_root().get_mouse_position()
	if __is_currently_using_drag_joystick:
#		Input.is_mouse_button_pressed(BUTTON_LEFT)
		fill_mouse_gui(__mouse_controls_pressure_gauge)
		var direction_vector = mouse_position - __mouse_controls_start_position
		if direction_vector.length_squared() > __mouse_controls_deadzone:
			var direction = Directions.get_direction_from_vector(direction_vector)
			
			add_to_mouse_gui_pressure_gauge(-1.62 * mouse_gui_fill_sensitivity)
			fill_mouse_gui(0)
			
			if self.is_in_limbo:
				release_direction_in_mouse_gui()
				return
			
			mark_direction_in_mouse_gui(direction)
			
			if is_cooling_down_between_actions():
				return
			
			var _has_moved = try_move(direction)
			# Let's try not checking if we have moved, and allow
			# passing turns when bumping into walls for example.
			# This makes the mouse implementation slighly different
			# from the keyboard implementation.
			# Think of it as AB testing.
			start_action_cooldown()
			spend_turn()
		else:
			add_to_mouse_gui_pressure_gauge(mouse_gui_fill_sensitivity)
			release_direction_in_mouse_gui()
#			show_mouse_gui_pressure_gauge()
			fill_mouse_gui(__mouse_controls_pressure_gauge)
		
		if __mouse_controls_pressure_gauge >= 1.0:
			__mouse_controls_pressure_gauge = 0.0
			spend_turn()
			perhaps_open_a_portal()
	else:
		add_to_mouse_gui_pressure_gauge(-1.62 * mouse_gui_fill_sensitivity)


func add_to_mouse_gui_pressure_gauge(how_much:float) -> void:
	__mouse_controls_pressure_gauge = min(
		1.0,
		max(
			0.0,
			__mouse_controls_pressure_gauge + how_much
		)
	)


#               _   _
#     /\       | | (_)
#    /  \   ___| |_ _  ___  _ __  ___
#   / /\ \ / __| __| |/ _ \| '_ \/ __|
#  / ____ \ (__| |_| | (_) | | | \__ \
# /_/    \_\___|\__|_|\___/|_| |_|___/
#
#

func try_move(direction) -> bool:
	var any_has_moved = false
	for item in get_items_that_are_you():
		var that_one_moved = try_move_item(item, direction)
		if that_one_moved:
			any_has_moved = true
		else:
			if item.is_weak:
				destroy_item(item)
	return any_has_moved


func try_move_item(item, direction, dry=false) -> bool:
	# Shenanigans to support UP and DOWN
	if direction == Directions.UP:
		if item.is_looking_right():
			direction = Directions.UP_RIGHT
		else:
			direction = Directions.UP_LEFT
	if direction == Directions.DOWN:
		if item.is_looking_right():
			direction = Directions.DOWN_RIGHT
		else:
			direction = Directions.DOWN_LEFT
	
	# Ok, let's look at what we have on the target tile
	var next_position = HexagonalLattice.find_adjacent_position(
		item.cell_position,
		direction
	)
	var next_items = get_items_on(next_position)
	
	# To simplify things, the YOU can only go where where are tiles on the map
	# This differs from BiY, but it keeps things simple and sparse.
	if not can_go_on(next_position):
		return false
	
	# Rule: STOP items block movement but only if they're not also PUSH
	for next_item in next_items:
		if next_item.is_stop and not next_item.is_push:
			return false
	
	# Rule: SHUT items block movement, unless you're OPEN
	for next_item in next_items:
		if not next_item.is_push and next_item.is_shut and not item.is_open:
			return false
	
	# Check if there's room to push
	var found_items_to_push = false
	for next_item in next_items:
		if next_item.is_push or next_item.is_text:
			found_items_to_push = true
			var can_next_move = try_move_item(next_item, direction, true)
			if not can_next_move:
				return false
	
	# Apply push movement on the items on the next tile
	if found_items_to_push and not dry:
		for next_item in next_items:
			if next_item.is_push or next_item.is_text:
				var can_next_move = try_move_item(next_item, direction, dry)
				assert(can_next_move, "Please tell me if this happens.")
				if not can_next_move:
					return false
	
	# Apply move to this item
	if not dry:
		item.cell_position = next_position
		if direction != item.direction:
			item.direction = direction
		item.advance_animation()
		item.update_sprite()
		item.raise_dust()
		item.reposition()
	
	return true


func reindex_lattice() -> void:
	cell_lattice.reindex_from_nodes(get_all_items())


const LevelCompleteParticles = preload("res://effects/LevelCompleteParticles.tscn")


func animate_victory(on_cells:Array) -> void:
	stop_accepting_inputs()
	
	# Particles, yummy particles
	var cpu_irrespect_threshold := 16
	var amount_of_emitters_created := 0
	for cell in on_cells:
		if amount_of_emitters_created > cpu_irrespect_threshold:
			break
		
		var particles = LevelCompleteParticles.instance()
		effects_layer.add_child(particles)
		particles.position = get_cell_map().map_to_world(cell)
		particles.restart()
		
		amount_of_emitters_created += 1
		
	# Show victory card
	# WiP — WiP EVERYWHERE, but especially here :]
#	yield(get_tree().create_timer(0.2), "timeout")
#	var victory_card = preload("res://guis/VictoryCard.tscn").instance()
#	$Dialogs.add_child(victory_card)


#  _      _           _
# | |    (_)         | |
# | |     _ _ __ ___ | |__   ___
# | |    | | '_ ` _ \| '_ \ / _ \
# | |____| | | | | | | |_) | (_) |
# |______|_|_| |_| |_|_.__/ \___/
#
#

signal limbo_entered
signal limbo_exited

var is_in_limbo = false


func enter_limbo():
#	$AnimationPlayer.play("Limbo", -1, 0.5)
	if not is_in_limbo:
		self.modulate = Color(0.9, 0.9, 0.9, 1.0)
		is_in_limbo = true
		emit_signal("limbo_entered")
		mouse_gui.show_right_mouse_button_hint()  # use signals instead


func exit_limbo():
	if is_in_limbo:
#		$AnimationPlayer.play("Limbo", -1, -1)
		self.modulate = Color(1.0, 1.0, 1.0, 1.0)
		is_in_limbo = false
		emit_signal("limbo_exited")
		mouse_gui.hide_mouse_button_hint()  # use signals instead


#  _____       _
# |  __ \     | |
# | |__) |   _| | ___  ___
# |  _  / | | | |/ _ \/ __|
# | | \ \ |_| | |  __/\__ \
# |_|  \_\__,_|_|\___||___/
#
#

signal turn_spending_started
signal turn_spending_ended


func can_go_on(that_hexagon_position):
	var map = get_cell_map()
	var is_emptiness = map.get_cellv(that_hexagon_position) == map.INVALID_CELL
	# … add more emptiness conditions here as-needed
	return not is_emptiness


#func compute_turn(): who cares how it is named?
func spend_turn(in_editor=false):

	emit_signal("turn_spending_started")

	# For faster computation, we want to index the items.
	# Here's a good place to reindex, since items may have moved.
	# We should probably (re-)index at the END of the turn.
	reindex_lattice()

	# I. Reset qualities and stuff, sentences will be re-applied
	reset_qualities_and_stuff()

	# II. Find all the possible sentences
	var sentences = get_possible_sentences()
	if not sentences.empty():
		print("Found %d sentences" % [sentences.size()])
		for sentence in sentences:
			print(sentence)
	else:
		print("No sentences found at all.")

	for sentence_object in sentences:
		for item in sentence_object.items_used:
			item.is_lit = true

	# III. Identity (THING IS THING) has utmost priority
	var things_locked = get_things_locked_to_identity(sentences)
	for sentence_object in sentences:
		if Words.VERB_IS != sentence_object.verb.concept_name:
			continue
		var locked_subjects = Array()
		var locked_subjects_concepts = Array()
		for subject in sentence_object.subjects:
			if things_locked.has(subject.concept_name) and not subject.negated:
				locked_subjects.append(subject)
				locked_subjects_concepts.append(subject.concept_name)
		if not locked_subjects:
			continue
		var locked_complements = Array()
		for complement in sentence_object.complements:
			if not complement.is_thing:
				continue
			if complement.negated:
				continue
			if locked_subjects_concepts.has(complement.concept_name):
				continue
			locked_complements.append(complement)
		if not locked_complements:
			continue
		# TODO: partial lock?
		if locked_complements.size() == sentence_object.complements.size():
			if locked_subjects.size() == sentence_object.subjects.size():
				for item in sentence_object.items_used:
					item.is_ignored = true
	
	# IV. Apply sentences
	for item in get_all_items():
		apply_qualifying_sentences_to_item(sentences, item)
	if not in_editor:
		for item in get_all_items():
			apply_stuffing_sentences_to_item(sentences, item)
		for item in get_all_items():
			apply_alchemical_sentences_to_item(sentences, item)
		for item in get_all_items():  # another qualifying pass, for created items?
			apply_qualifying_sentences_to_item(sentences, item)
	
	# VIII. Same-tile effects
	var has_won = false
	if not in_editor:
		apply_weak_effect()
		apply_sink_effect()
		apply_hot_effect()
		has_won = apply_win_effect()
		if not has_won:
			apply_lose_effect()
			apply_open_effect()

	# more special cases
	# …

	# XX. Defeat|Limbo Scenario : THERE IS NO YOU
	if not in_editor and not has_won:
		var no_you = true
		for item in get_all_items():
			if item.is_you:
				no_you = false
				break
		if no_you:
			enter_limbo()

	# Reposition all the items?
	# …?

	# Update the items aesthetics from their new flags
	for item in get_all_items():
#		item.update_aesthetics()
		item.update_particles()
#		item.update_z_salience()

	# L. Reregister all the items in case we created some.
	# This could be optimized, if necessary, by registering only when needed.
	register_all_items()

	# LI. Write this turn into the history ledger
	write_history()
	
	# C. Emit signals !
	emit_signal("turn_spending_ended")
	
	# Meanwhile…
	update_items_z_salience()


func get_applicable_sentences(sentences:Array, item:Item) -> Array:
	var applicable_sentences = Array()
	for sentence in sentences:
		var is_a_subject = false
		
		for subject in sentence.subjects:
			if subject.negated:
				if subject.concept_name != item.concept_name:
					is_a_subject = true
					break
			else:
				if subject.concept_name == item.concept_name:
					is_a_subject = true
					break
		
		if not is_a_subject:
			continue
		
		applicable_sentences.append(sentence)
	
	return applicable_sentences


func get_things_locked_to_identity(sentences:Array) -> Array:
	var is_item_locked_to_identity = false
	var concepts_locked = Array()
	
	for sentence in sentences:
		if Words.VERB_IS != sentence.verb.concept_name:
			continue
		for subject in sentence.subjects:
			if not subject.is_thing:
				assert(false, "Wait…  What?  No!")
				continue
			if subject.negated:
				continue
			for complement in sentence.complements:
				if not complement.is_thing:
					continue
				if complement.negated:
					continue
				if complement.concept_name == subject.concept_name:
					concepts_locked.append(subject.concept_name)
	
	return concepts_locked


func apply_qualifying_sentences_to_item(sentences:Array, item:Item) -> void:
	if item.is_text:
		return
	item.reset_properties()
	var applicable_sentences = get_applicable_sentences(sentences, item)
	for sentence in applicable_sentences:
		if Words.VERB_IS != sentence.verb.concept_name:
			continue
		var qualities_to_acquire := Array()
		for complement in sentence.complements:
			if complement.is_quality:
				qualities_to_acquire.append(complement.concept_name)
		if qualities_to_acquire:
			for quality in qualities_to_acquire:
				item.qualify(quality)


func apply_alchemical_sentences_to_item(sentences:Array, item:Item) -> void:
	if item.is_text:
		return
	var applicable_sentences = get_applicable_sentences(sentences, item)
	var is_item_locked_to_identity = false
	
	for sentence in applicable_sentences:
		if Words.VERB_IS != sentence.verb.concept_name:
			continue
		for complement in sentence.complements:
			if not complement.is_thing:
				continue
			if complement.negated:
				continue
			if complement.concept_name == item.concept_name:
				is_item_locked_to_identity = true
	
	if is_item_locked_to_identity:
		return
	
	for sentence in applicable_sentences:
		if Words.VERB_IS != sentence.verb.concept_name:
			continue
		var things_to_become := Array()
		for complement in sentence.complements:
			if complement.is_thing:
				if complement.negated:
					for other_thing in __unique_things_available:
						if other_thing == complement.concept_name:
							continue
						things_to_become.append(other_thing)
				else:
					things_to_become.append(complement.concept_name)
		
		if things_to_become:
			item.transmute(things_to_become.pop_front())
		if things_to_become:
			for thing_to_become in things_to_become:
				var created_item = Game.spawn_item(item, {
					'concept_name': thing_to_become,
				})


func apply_stuffing_sentences_to_item(sentences:Array, item:Item) -> void:
	if item.is_text:
		return
	var applicable_sentences = get_applicable_sentences(sentences, item)
	for sentence in applicable_sentences:
		if Words.VERB_HAS != sentence.verb.concept_name:
			continue
		for complement in sentence.complements:
			if complement.is_thing:
				item.stuff_with(complement.concept_name)


################################################################################

#  _____                               _
# |  __ \                             | |
# | |__) |___ _ __ ___   ___ _ __ ___ | |__  _ __ __ _ _ __   ___ ___
# |  _  // _ \ '_ ` _ \ / _ \ '_ ` _ \| '_ \| '__/ _` | '_ \ / __/ _ \
# | | \ \  __/ | | | | |  __/ | | | | | |_) | | | (_| | | | | (_|  __/
# |_|  \_\___|_| |_| |_|\___|_| |_| |_|_.__/|_|  \__,_|_| |_|\___\___|
#
# 

# All Items (including the destroyed ones) are indexed here.
# We use this to fetch past Items during undo for example.
# This will also keep the past Items safe from garbage collection,
# once they are removed from the scene tree.
var items_bag = Dictionary()  # instance_id => Item


# The history of turns spent on this level.
# Undo truncates the last entry.
# We could use this upon victory to show a replay, that would be awesome.
var history = Array()  # of Dictionary(instance_id => item_pickle)


func hash_item(item):
	return item.get_instance_id()


func register_all_items():
	for item in get_all_items():
		register_item(item)


func register_item(item):
	assert(item is Item)
	var key = hash_item(item)
	if key in self.items_bag:
		assert(item == self.items_bag[key])
		return
	self.items_bag[key] = item


func write_history():
	var this_turn = Dictionary()
	for item in get_all_items():
		this_turn[hash_item(item)] = item.to_pickle()
	history.append(this_turn)


func undo():
	if 2 > self.history.size():
		return
	call_deferred('__deferred_undo')


func __deferred_undo():
	for item in get_all_items():
		self.items_layer.remove_child(item)
	var previous_turn = self.history[self.history.size() - 2]
	for item_hash in previous_turn.keys():
		assert(item_hash in self.items_bag)
		var item = self.items_bag[item_hash]
		item.from_pickle(previous_turn[item_hash])
		self.items_layer.add_child(item)
		item.reposition()
		item.update_aesthetics()

	self.history.pop_back()
	
	#perhaps_exit_limbo()
	exit_limbo()


#  _____           _        _
# |  __ \         | |      | |
# | |__) |__  _ __| |_ __ _| |___
# |  ___/ _ \| '__| __/ _` | / __|
# | |  | (_) | |  | || (_| | \__ \
# |_|   \___/|_|   \__\__,_|_|___/
#
#


func perhaps_open_a_portal() -> void:
	for portal in get_all_portals():
		var opened : bool = portal.perhaps_open()
		if opened:
			break


const Portal = preload("res://addons/laec-is-you/entity/Portal.gd")


func get_all_portals() -> Array:
	var portals = Array()
	for item in self.items_layer.get_children():
		if item is Portal and item.is_latticeable():
			portals.append(item)
	return portals


#  _____ _
# |_   _| |
#   | | | |_ ___ _ __ ___  ___
#   | | | __/ _ \ '_ ` _ \/ __|
#  _| |_| ||  __/ | | | | \__ \
# |_____|\__\___|_| |_| |_|___/
#
#

func get_unique_things_available() -> Array:  # of String concepts
	var unique_things = Array()
	for concept in get_unique_concepts_available():
		if not Words.is_thing(concept):
			continue
		unique_things.append(concept)
	return unique_things


func get_unique_concepts_available() -> Array:  # of String concepts
	var unique_concepts = Array()
	for item in get_all_items():
		var concept = item.get_concept_name()
		if unique_concepts.has(concept):
			continue
		unique_concepts.append(concept)
	#print(unique_concepts)
	return unique_concepts


func get_things(thing_name=null, is_text=null):
	var things = Array()
	for item in get_all_items():
		if item.is_thing:
			if null != thing_name and item.thing_name != thing_name:
				continue
			if null != is_text and item.is_text != is_text:
				continue
			things.append(item)
	return things


func get_all_items() -> Array:
	var items = Array()
	for item in self.items_layer.get_children():
		if item is Item and item.is_latticeable():
			items.append(item)
	return items


func get_items_on(that_cell_position:Vector2) -> Array:
	var items = get_all_items()
	var found_items = Array()
	for item in items:
		if item.cell_position == that_cell_position:
			found_items.append(item)
	return found_items


func get_items_with_qualities(qualities_filter):
	var items = Array()
	for item in get_all_items():
		var validates_filter = true
		for quality in qualities_filter.keys():
			var expected_value = qualities_filter[quality]
			if item.get('is_'+quality) != expected_value:
				validates_filter = false
				break
		if validates_filter:
			items.append(item)
	return items


func get_text_items():
	var text_items = Array()
	for item in get_all_items():
		if item.is_text:
			text_items.append(item)
	return text_items


func get_items_that_are_you():
	var you_items = Array()
	for item in get_all_items():
		if item.is_you:
			you_items.append(item)
	return you_items


func get_items_piled_with(item, including_item=false):
	var all_items = self.cell_lattice.find_on_cell(item.cell_position)
	if not including_item:
		all_items.remove(all_items.find(item))
	return all_items


func get_things_piled_with(item, including_item=false):
	var piled_things = Array()
	if including_item and item.is_thing:
		piled_things.append(item)
	var piled_items = get_items_piled_with(item, false)
	for piled_item in piled_items:
		if item.is_thing and not item.is_text:
			piled_things.append(piled_item)
	return piled_things


#   _____            _
#  / ____|          | |
# | (___   ___ _ __ | |_ ___ _ __   ___ ___  ___
#  \___ \ / _ \ '_ \| __/ _ \ '_ \ / __/ _ \/ __|
#  ____) |  __/ | | | ||  __/ | | | (_|  __/\__ \
# |_____/ \___|_| |_|\__\___|_| |_|\___\___||___/
#
#

func get_possible_sentences() -> Array:
	var possible_sentences = Array()
	var items = get_text_items()
	for item in items:
		var sentences = get_possible_sentences_from(item.hexagon_position)
		possible_sentences = possible_sentences + sentences
	
	for i in range(possible_sentences.size()-1, -1, -1):
		var sentence_inspected = possible_sentences[i]
		for sentence_object in possible_sentences:
			if sentence_inspected == sentence_object:
				continue
			if sentence_object.contains_sentence(sentence_inspected):
				possible_sentences.remove(i)
				break
	
	return possible_sentences

func get_possible_sentences_from(hexagon_position) -> Array:
	return (
		get_possible_sentences_in_direction(hexagon_position, Directions.RIGHT)
		+
		get_possible_sentences_in_direction(hexagon_position, Directions.DOWN_RIGHT)
	)

func get_possible_sentences_in_direction(hexagon_position:Vector2, direction) -> Array:
	var items := Array()
	var sentences := Array()
	var current_cell = hexagon_position
	
	for item in self.cell_lattice.find_at(current_cell):
		if not item.is_text:
			continue
		sentences.append([item])
	
	if sentences.empty():
		return sentences
	
	var there_is_more = true
	while there_is_more:
		current_cell = self.cell_lattice.find_adjacent_position(
			current_cell, direction
		)
		items = self.cell_lattice.find_at(current_cell)
		there_is_more = false
		var new_sentences := Array()
		for item in items:
			if not item.is_text:
				continue
			there_is_more = true
			for sentence in sentences:
				new_sentences.append(sentence + [item])
		if there_is_more:
			sentences = new_sentences
	
	# Prune sentences of less than three words
	for i in range(sentences.size()-1, -1, -1):
		if 3 > sentences[i].size():
			sentences.remove(i)
	
	var sentences_as_objects := Array()
	for sentence in sentences:
		var sentence_as_object = Sentence.new()
		sentence_as_object.from_items(sentence)
		if sentence_as_object.is_valid:
			sentences_as_objects.append(sentence_as_object)
	
	return sentences_as_objects

################################################################################

func apply_win_effect() -> bool:
	var has_won = false
	var winning_cells = Array()
	var cells = self.cell_lattice.get_used_cells()
	for cell in cells:
		var items_piled = self.cell_lattice.find_on_cell(cell)
		var found_win = false
		var found_you = false
		for item in items_piled:
			if item.is_win:
				found_win = true
			if item.is_you:
				found_you = true
		if found_win and found_you:
			has_won = true
			winning_cells.append(cell)

	if has_won:
		print("GRATULO !  %s IS WIN" % self.name.to_upper())
		register_victory()
		animate_victory(winning_cells)
		set_completed(true)
		emit_signal("level_completed", self, winning_cells)
		
		yield(get_tree().create_timer(2.0), "timeout")
		
		Game.go_to_level_victory_screen(self)
		
	return has_won


var __is_completed := false


func set_completed(completion_status:bool) -> void:
	self.__is_completed = completion_status


func is_completed() -> bool:
	return self.__is_completed


signal level_completed(level, winning_cells)


func apply_lose_effect():
	var applied = false
	for item in get_items_with_qualities({Words.QUALITY_DEFEAT: true}):
		var piled_items = get_items_piled_with(item)
		for other_item in piled_items:
			if other_item.is_you:
				destroy_item(other_item)
				applied = true
	return applied

func apply_sink_effect():
	for item in get_items_with_qualities({Words.QUALITY_SINK: true}):
		var piled_items = get_items_piled_with(item)
		for other_item in piled_items:
			destroy_item(other_item)
			destroy_item(item)
			break

func apply_weak_effect():
	for item in get_items_with_qualities({Words.QUALITY_WEAK: true}):
		var piled_items = get_items_piled_with(item)
		if not piled_items.empty():
			destroy_item(item)

func apply_open_effect():
	var applied = false
	for item in get_items_with_qualities({Words.QUALITY_OPEN: true}):
		var piled_items = get_items_piled_with(item)
		for other_item in piled_items:
			if other_item.is_shut:
				destroy_item(other_item)
				destroy_item(item)
				applied = true
				break
	return applied

func apply_hot_effect():
	var applied = false
	for item in get_items_with_qualities({Words.QUALITY_HOT: true}):
		var piled_items = get_items_piled_with(item)
		for other_item in piled_items:
			if other_item.is_melt:
				destroy_item(other_item)
				applied = true
	return applied

################################################################################


#  _   _ _ _   _                 _____      _ _   _
# | \ | (_) | | |               / ____|    (_) | | |
# |  \| |_| |_| |_ _   _ ______| |  __ _ __ _| |_| |_ _   _
# | . ` | | __| __| | | |______| | |_ | '__| | __| __| | | |
# | |\  | | |_| |_| |_| |      | |__| | |  | | |_| |_| |_| |
# |_| \_|_|\__|\__|\__, |       \_____|_|  |_|\__|\__|\__, |
#                   __/ |                              __/ |
#                  |___/                              |___/

func reset_qualities_and_stuff():
	for item in get_all_items():
		item.reset_properties()

func destroy_item(item:Item):
	# … emit signals everywhere !
	
	remove_item_from_lattice(item)
	
#	if item.is_in_movement():
		# wait
	
	item.animate_destruction()
	
	var remains = item.stuffing
	for remaining_thing_name in remains:
		var remaining_thing = Game.spawn_item(item, {
			'concept_name': remaining_thing_name
		})
		# Now, if we apply the sentences to the created object,
		# beware of infinite loops!
		# …
	reindex_lattice()  # expensive!
	var delay = get_action_cooldown()
#	var delay = get_particles_lifetime() ?
	yield(get_tree().create_timer(delay), "timeout")
	remove_item_from_scene(item)
	
	# UNDO fetches the Item back from the bin.
	# The Item has been removed from the scene tree, so
	# it is safe to mark the Item as ~latticeable now.
	item.set_latticeability(true)


func remove_item_from_scene(item):
	items_layer.remove_child(item)


func remove_item_from_lattice(item):
	item.set_latticeability(false)
#	cell_lattice.remove_item(item)  # see reindex monkey fix


func update_items_z_salience():
	pass
	# See Item.set_position(), where we bypass the z-dragon.
#	for item in get_all_items():
#		item.position.y += item.get_z_salience() * 0.01
#		item.z_index = item.get_z_salience()


################################################################################

func register_victory():
	Game.register_victory()
