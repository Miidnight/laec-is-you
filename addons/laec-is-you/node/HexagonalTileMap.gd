tool
extends TileMap

# HexagonalTileMap
# ----------------
# 
# Auto-configure a TileMap for simple hexagonal tiling
# and perhaps also provide some goodies.
# 
# - Aligns the origins between hex and pixel
# - Friendly Kenney friendly  https://www.kenney.nl/assets?s=hexagon

const XY_RATIO = 0.8660254037844386  # Math.sqrt(3) / 2.0

# Only "square" hexs for simplicity with this parameter.
export(float) var hex_size = 64.0 setget set_hex_size, get_hex_size
# Like this, or experiment with transforms? Rotations would be nice too.
#export(float) var hex_size_x = 64.0 setget set_hex_size, get_hex_size
#export(float) var hex_size_y = 64.0 setget set_hex_size, get_hex_size

# The center gets slightly offseted to match the sprites
# Godot complains this variable is not used when it IS used in a ternary.
export(bool) var kenney_tiles_friendly = true

func _ready():
	reconfigure()

func get_hex_size():
	return hex_size

func set_hex_size(new_value):
	hex_size = new_value
	reconfigure()

func reconfigure():
	mode = MODE_CUSTOM
	cell_size = Vector2(hex_size, hex_size)  # ghost?
	cell_custom_transform = Transform2D(
		Vector2(hex_size, 0),
		Vector2(0, hex_size * XY_RATIO),
		Vector2(-0.5 * hex_size, -0.5 * hex_size * XY_RATIO)
	)
	cell_half_offset = TileMap.HALF_OFFSET_X

# Patch that to yield the center of the hexagon, for kenney or others.
# This is a confused workaround for cell_tile_origin to center being confusing
func map_to_world(hexagon_position, ignore_half_offset=false):
	var koff = 0
	if kenney_tiles_friendly:
		koff = 1
#	var koff = 1 if self.kenney_tiles_friendly else 0
	return .map_to_world(hexagon_position, ignore_half_offset) \
			+ Vector2(
				hex_size * 0.5,
				hex_size * XY_RATIO / (2.0 + koff)
			)
