tool
extends EditorPlugin

const PATH_CELL_CURSOR = "res://guis/HexagonGui.tscn"
const PATH_LAEC_ADDON = "res://addons/laec-is-you/"
const PATH_ITEM_SELECTOR = PATH_LAEC_ADDON + "ide/ItemSelector.tscn"
const PATH_ITEM_WIZARD = PATH_LAEC_ADDON + 'ide/ItemWizard.tscn'
const PATH_LEVEL_BOILERPLATE = PATH_LAEC_ADDON + 'core/BoilerplateLevel.tscn'
const PATH_LEVEL_OUTPUT = 'res://levels'
const PATH_EDITOR_GRID_TEXTURE = "res://addons/laec-is-you/ide/editor_grid.png"
const PATH_ITEM_SCENE = "res://core/Item.tscn"
const PATH_ITEM_SCRIPT = "res://addons/laec-is-you/entity/Item.gd"
const PATH_PORTAL_SCRIPT = "res://addons/laec-is-you/entity/Portal.gd"
const PATH_LEVEL_SCRIPT = "res://addons/laec-is-you/entity/Level.gd"
const PATH_TILE_MAP_SCRIPT = 'node/HexagonalTileMap.gd'
const PATH_HEX_TEXTURE = 'icon/hexagon.svg'
const PATH_ITEM_DIRECTORY = "res://sprites/items"
# Appears in the top menu of the 2D Editor (press F1)
var container = CONTAINER_CANVAS_EDITOR_MENU
var items : Array
var items_by_name : Dictionary
var item_wizard_control
var refresh_button
var new_level_button : Button
var add_button : MenuButton
var more_button : MenuButton
var text_menu = PopupMenu.new()
var editor_grid = recreate_editor_grid()
var item_selector : Node = null
var cell_cursor = preload(PATH_CELL_CURSOR).instance()
var ItemSelector = preload(PATH_ITEM_SELECTOR).instance()
var was_handling : bool = false

#  _____ _                  __          ___                  _
# |_   _| |                 \ \        / (_)                | |
#   | | | |_ ___ _ __ ___    \ \  /\  / / _ ______ _ _ __ __| |
#   | | | __/ _ \ '_ ` _ \    \ \/  \/ / | |_  / _` | '__/ _` |
#  _| |_| ||  __/ | | | | |    \  /\  /  | |/ / (_| | | | (_| |
# |_____|\__\___|_| |_| |_|     \/  \/   |_/___\__,_|_|  \__,_|
#

#const Level = preload('entity/Level.gd')

func parse_name(file_name: String):
	if file_name.ends_with("_0.png"):
		var regex = RegEx.new()
		regex.compile(
			"(?<is_text>text_)?" + 
			"(?<item_name>[A-Za-z0-9-]+)?_?" +
			
			"(?<direction>(bottom_left|bottom_right|left|right|top_left|top_right))?_?" +
			"(?<animation>[0-9]+)?_?" +
			"(?<frisson>[0-9]+)?_?" +
			
			"[.]png$"
		)
		var result = regex.search(file_name)
		
		if result:
			var item = Dictionary()
			item["file_name"] = file_name
			item["concept"] = result.get_string("item_name")
			if result.get_string("direction") != "":
				item["direction"] = result.get_string("direction")
			else:
				item["direction"] = null
			item["is_text"] = result.get_string("is_text") == "text_"
			if result.get_string("animation") != "":
				item["animation"] = result.get_string("animation")
			else:
				item["animation"] = null
			if result.get_string("frisson") != "":
				item["frisson"] = result.get_string("frisson")
			else:
				item["frisson"] = null
			return item

	
	
func get_items_from_sprites_dir(path):
	var dir = Directory.new()
	var results = []
	if dir.open(path) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				print("Found directory: " + file_name)
			else:
				var item = parse_name(file_name)
				if item:
					results.push_back(item)
			file_name = dir.get_next()
		return results
		
	else:
		print("An error occurred when trying to access the path.")
	return null

func sort_items(p_items: Array):
	items_by_name = Dictionary()
	var already_parsed_items = []
	var sorted_items = []
	for item in p_items:
		if already_parsed_items.find(item["concept"]) == -1 and not item["is_text"]:
			sorted_items.push_back(item)
			items_by_name[item["concept"]] = item
			already_parsed_items.push_back(item["concept"])
		if item["is_text"]:
			sorted_items.push_back(item)
			items_by_name["text_" + item["concept"]] = item
	return sorted_items

func add_items_tree(p_items: Array):
	add_button.get_popup().clear()
	text_menu.name = "text"
	if not text_menu.get_parent() == add_button.get_popup():
		add_button.get_popup().add_child(text_menu)
	add_button.get_popup().add_submenu_item(
		"text", "text"
	)
	for item in p_items:
		if item["is_text"]:
			text_menu.add_item(item["concept"])
			
		else:
			add_button.get_popup().add_item(item["concept"])

func are_all_selected_nodes_script(script_path : String):
	for selection in get_editor_interface().get_selection().get_selected_nodes():
		if selection.script:
			if selection.script.resource_path != script_path:
				return false
		else:
			return false
	if get_editor_interface().get_selection().get_selected_nodes().size() > 0:
		return true
	else:
		return false

func are_all_selected_nodes_items():
	return are_all_selected_nodes_script(PATH_ITEM_SCRIPT)

func are_all_selected_nodes_portals():
	return are_all_selected_nodes_script(PATH_PORTAL_SCRIPT)

func is_editing_a_level():
	var scene_root = get_editor_interface().get_edited_scene_root()
	if scene_root:
		if scene_root.script:
			return scene_root.script.resource_path == "res://addons/laec-is-you/entity/Level.gd"
	else:
		return false

var was_handling_type
const HANDLE_TYPE_PORTAL = 0
const HANDLE_TYPE_ITEM = 1

func has_to_be_handled():
	if (
		is_editing_a_level() and
		(
			are_all_selected_nodes_items() or 
			are_all_selected_nodes_portals()
		)
	):
		return true
	return false

func should_be_refresh():
	if (
		not was_handling 
		or
		(
			(
				are_all_selected_nodes_portals() 
				and 
				not was_handling_type == HANDLE_TYPE_PORTAL
			)
			or
			(
				not are_all_selected_nodes_portals() 
				and 
				was_handling_type == HANDLE_TYPE_PORTAL
			)
		)
	):
		return true
	return false

func handles(object: Object):
#	print("was handling:%s" % [was_handling])
	if has_to_be_handled():
		if should_be_refresh():
			_init_level_editor()
			print("now handling")
			if are_all_selected_nodes_portals():
				was_handling_type = HANDLE_TYPE_PORTAL
				item_selector.visible = false
			else:
				was_handling_type = HANDLE_TYPE_ITEM
				print("item selector now visible")
				item_selector.call_deferred("set_visible", true)
		was_handling = true
#		print("is handling:%s" % [true])
		return true
	if was_handling:
		_clean_level_editor()
#	print("is handling:%s" % [false])
	was_handling = false
	return false

func recreate_cell_cursor():
	cell_cursor = preload(PATH_CELL_CURSOR).instance()

func recreate_editor_grid():
	editor_grid = Sprite.new()
	editor_grid.position = Vector2(-5.0, 11.0)
	editor_grid.texture = preload(PATH_EDITOR_GRID_TEXTURE)
	editor_grid.scale = Vector2(0.984, 1.131)
	editor_grid.region_enabled = true
	editor_grid.region_rect = Rect2(0.0, 0.0, 2000.0, 2000.0)

func recreate_item_selector():
	item_selector = load(PATH_ITEM_SELECTOR).instance()

func _init_level_editor():
	var scene_root = get_editor_interface().get_edited_scene_root()
	var canvasViewport = get_editor_interface().get_editor_viewport()
	if not cell_cursor:
		recreate_cell_cursor()
	if not editor_grid:
		recreate_editor_grid()
	if not cell_cursor.get_parent():
		cell_cursor.scale = Vector2(0.827, 0.827)
		scene_root.add_child(cell_cursor)
	if not editor_grid.get_parent():
		cell_cursor.scale = Vector2(0.827, 0.827)
		scene_root.add_child(editor_grid)
	if not item_selector:
		recreate_item_selector()
	if not item_selector.get_parent():
		print("adding to viewport the item selector")
		item_selector.set_level_editor(self)
		add_control_to_container(
			EditorPlugin.CONTAINER_CANVAS_EDITOR_SIDE_RIGHT,
			item_selector
		)
		item_selector.fill_item_container()
	cell_cursor.visible = more_button.get_popup().is_item_checked(MORE_OPTION_DISPLAY_CURSOR)
	editor_grid.visible = more_button.get_popup().is_item_checked(MORE_OPTION_DISPLAY_GRID)
#	more_button.visible = true
#	add_button.visible = true
#	refresh_button.visible = true
	var color_rect = ColorRect.new()
	var center_container = CenterContainer.new()
	
	center_container.add_child(color_rect)
	scene_root.get_parent().get_parent().add_child(center_container)
	color_rect.margin_bottom = 0
	color_rect.margin_top = 0
	color_rect.margin_left = 0
	color_rect.margin_right = 0
	
func _clean_level_editor():
	remove_control_from_container(
		EditorPlugin.CONTAINER_CANVAS_EDITOR_SIDE_RIGHT,
		item_selector
	)
	item_selector.queue_free()
#	more_button.visible = false
#	add_button.visible = false
#	refresh_button.visible = false

func index_items():
	var found_items = get_items_from_sprites_dir(PATH_ITEM_DIRECTORY)
	self.items = sort_items(found_items)
	add_items_tree(self.items)

func setup_item_wizard():
	item_wizard_control = preload(PATH_ITEM_WIZARD).instance()
	refresh_button = item_wizard_control.find_node("Refresh")
	add_button = item_wizard_control.find_node("Add")
	more_button = item_wizard_control.find_node("More")
	new_level_button = item_wizard_control.find_node("NewLevel")
	
	more_button.get_popup().add_check_item("display_grid")
	more_button.get_popup().add_check_item("display_cursor")
	
	add_button.get_popup().connect('index_pressed', self, 'add_item_pressed')
	text_menu.connect('index_pressed', self, 'add_text_pressed')
	refresh_button.connect('pressed', self, 'refresh_pressed')
	new_level_button.connect('pressed', self, 'new_level_pressed')
	more_button.get_popup().connect('index_pressed', self, 'more_option_pressed')
	
	add_control_to_container(container, item_wizard_control)

func _enter_tree():
	add_custom_type(
		"HexagonalTileMap", "TileMap",
		preload(PATH_TILE_MAP_SCRIPT),
		preload(PATH_HEX_TEXTURE)
	)
	setup_item_wizard()
	index_items()

func _exit_tree():
	if item_selector:
		item_selector.queue_free()
	if cell_cursor:
		cell_cursor.queue_free()
	if add_button:
		add_button.queue_free()
	if refresh_button:
		refresh_button.queue_free()
	remove_custom_type("HexagonalTileMap")
	remove_control_from_container(container, item_wizard_control)

func forward_canvas_gui_input(event):
	var consumed = false
	on_canvas_editor_input(event)
	return consumed

func on_canvas_editor_input(event):
	var scene_root = get_tree().get_edited_scene_root()
	var mouse_coords = scene_root.get_global_mouse_position()
	var tilemap : TileMap = get_editor_interface().get_edited_scene_root().find_node("HexagonalTileMap")
	var cell = tilemap.world_to_map(mouse_coords)
	if not cell_cursor:
		recreate_cell_cursor()
	if not editor_grid:
		recreate_editor_grid()
	cell_cursor.position = tilemap.map_to_world(cell)

var file_dialog : FileDialog

func new_level_pressed():
	print("adding new level")
	file_dialog = FileDialog.new()
	file_dialog.set_access(FileDialog.ACCESS_RESOURCES)
	file_dialog.set_mode(FileDialog.MODE_SAVE_FILE)
	file_dialog.set_current_dir(PATH_LEVEL_OUTPUT)
	file_dialog.add_filter("*.tscn ; Scene File")
	file_dialog.connect("file_selected", self, "create_new_level")
	
	file_dialog.anchor_bottom = 1.0
	file_dialog.anchor_right = 1.0
	file_dialog.call_deferred("set_visible", true)
	file_dialog.call_deferred("update")
	file_dialog.call_deferred("set_current_dir", PATH_LEVEL_OUTPUT)

	get_editor_interface().get_editor_viewport().add_child(file_dialog)
	print(file_dialog)

func create_new_level(file_path : String):
	var dir = Directory.new()
	dir.copy(PATH_LEVEL_BOILERPLATE, file_path)
	get_editor_interface().open_scene_from_path(file_path)
	if file_dialog:
		file_dialog.queue_free()
	
func refresh_pressed():
	print('[ItemWizard] \\ô/`')
	var level = get_editor_interface().get_edited_scene_root()
	var LevelType = load(PATH_LEVEL_SCRIPT)
	if not (level is LevelType):
		printerr(
			"You may only use this button on Level scenes.\n"+
			"We will hide the button contextually, eventually.\n"+
			"It's a good first contribution ;)"
		)
		return
	
	var amount_snapped = 0
	for item in level.get_all_items():
		item.infer_position()
		amount_snapped += 1
	if 0 < amount_snapped:
		print("[ItemWizard] Snapped %d Items." % amount_snapped)

	print("[ItemWizard] Passing turn but only with some of the rules.")
	level.spend_turn(true)

	var amout_updated = 0
	for item in level.get_all_items():
		item.update_sprite()
		item.rename()
		amout_updated += 1
	if 0 < amout_updated:
		print("[ItemWizard] Updated %d Item sprites." % amout_updated)

# ADDING ITEMS

func add_item_to_edited_scene(concept : String, is_text : bool = false):
	if not get_editor_interface().get_edited_scene_root():
		printerr("Please open a Level scene first in res://levels")
		return
	var item_container = get_editor_interface().get_edited_scene_root().find_node("Items")
	if not item_container:
		printerr("Please open a Level scene first in res://levels")	
		return
	var item : Dictionary
	var item_name : String
	if is_text:
		item_name = "text_" + concept
	else:
		item_name = concept
	item = items_by_name[item_name]
	
	var Item = preload(PATH_ITEM_SCENE)
	var item_node = Item.instance()
	
	if item["direction"]:
		item_node.direction = item["direction"]
	else:
		item_node.direction = "right"
	item_node.is_text = is_text
	item_node.animation_state = 0
	item_node.concept_name = item["concept"]
	item_container.add_child(item_node)
	
	item_node.set_owner(get_editor_interface().get_edited_scene_root())
#	get_editor_interface().get_selection().add_node(item_node)
	print(get_editor_interface().get_selection().get_selected_nodes())
	refresh_pressed()
	print("%s has been added to the scene" % [ item_name ])
	yield(get_tree(),"idle_frame")
	get_editor_interface().get_selection().call_deferred("clear")
#	clear()
	get_editor_interface().get_selection().call_deferred("add_node", item_node)

func add_item_pressed(id : int):
	add_item_to_edited_scene(
		add_button.get_popup().get_item_text(id)
	)

func add_text_pressed(id : int):
	add_item_to_edited_scene(
		text_menu.get_item_text(id),
		true
	)

const MORE_OPTION_DISPLAY_GRID = 0
const MORE_OPTION_DISPLAY_CURSOR = 1
func more_option_pressed(id : int):
	if id == MORE_OPTION_DISPLAY_GRID:
		more_button.get_popup().set_item_checked(
			id,
			not more_button.get_popup().is_item_checked(id)
		)
		editor_grid.visible =  more_button.get_popup().is_item_checked(id)
	elif id == MORE_OPTION_DISPLAY_CURSOR:
		more_button.get_popup().set_item_checked(
			id,
			not more_button.get_popup().is_item_checked(id)
		)
		cell_cursor.visible =  more_button.get_popup().is_item_checked(id)
		
