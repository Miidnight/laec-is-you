class_name HexagonalLattice

# Hasty, incomplete abstraction of a lattice.
# It does not know about the size of hexagons; and does not care.
# See HexagonalTileMap for conversion methods.
# 
# This is essentially a wrapper for a Dictionary,
# storing a list of Items for each cell (Vector2).
# 
# This is BAD.  Do NOT use at home.
# - Tied to nodes having cell_position as attribute
# - Dumb offset-mode coords T_T
# 
# To make this better,
# we'd also need a rewrite of the HexagonalTileMap with proper coords,
# but that would require careful bridging with Godot's TileMap API.
# I made one, but I lost it.  I can't find the energy to remake it right now.
# 
# Therefore, this. ugly. piece. of. garbage.

# Vector2 cell (dumb offset-mode coords) => Array of Nodes
var map = Dictionary()

func reset_map():
	map.clear()

func get_used_cells():
	return self.map.keys()

func find_on_cell(cell):
	return find_at(cell)

func find_at(cell_position):
	return self.map.get(cell_position, Array())

func add_node(node):
	var where = node.cell_position  # /!.  node is Item
	if not self.map.has(where):
		self.map[where] = Array()
	self.map[where].append(node)

func reindex_from_nodes(nodes):
	reset_map()
	for node in nodes:
		add_node(node)


#  _______          _ _
# |__   __|        | (_)
#    | | ___   ___ | |_ _ __   __ _
#    | |/ _ \ / _ \| | | '_ \ / _` |
#    | | (_) | (_) | | | | | | (_| |
#    |_|\___/ \___/|_|_|_| |_|\__, |
#                              __/ |
#                             |___/
# 

#const ODDR_DIRECTIONS = [
#	[[+1,  0], [ 0, -1], [-1, -1], 
#	[-1,  0], [-1, +1], [ 0, +1]],
#	[[+1,  0], [+1, -1], [ 0, -1], 
#	[-1,  0], [ 0, +1], [+1, +1]],
#]
# x at 3 o'clock
# y at 7 o'clock (about)
const ODDR_DIRECTIONS = [
	{
		Directions.RIGHT: [+1,  0],
		Directions.UP_RIGHT: [0, -1],
		Directions.UP_LEFT: [-1, -1],
		Directions.LEFT: [-1, 0],
		Directions.DOWN_LEFT: [-1, +1],
		Directions.DOWN_RIGHT: [0, +1],
	},
	{
		Directions.RIGHT: [+1,  0],
		Directions.UP_RIGHT: [+1, -1],
		Directions.UP_LEFT: [0, -1],
		Directions.LEFT: [-1, 0],
		Directions.DOWN_LEFT: [0, +1],
		Directions.DOWN_RIGHT: [+1, +1],
	},
]


static func find_adjacent_position(cell_position, direction):
	var parity = int(cell_position.y) & 1
	var dir = ODDR_DIRECTIONS[parity][direction]
	return Vector2(cell_position.x + dir[0], cell_position.y + dir[1])
