tool
extends Control

const SIZE_ICON_BASE = 64.0

onready var _item_container : ItemList = get_node("ItemListContainer/ItemContainer")
onready var _direction_container : ItemList = get_node("ItemListContainer/DirectionContainer")
onready var _item_sizer : HSlider = get_node("ItemSizer")
onready var _item_search : LineEdit = get_node("SearchBox")
onready var _repaint_button : Button = get_node("Repaint")
onready var _rescan_button : Button = get_node("Rescan")


var level_editor : EditorPlugin setget set_level_editor

func set_level_editor(p_level_editor: EditorPlugin):
	level_editor = p_level_editor
	
func reset_bg_colors():
	for i in range(_item_container.get_item_count()):
		_item_container.set_item_custom_bg_color(i, Color(0, 0, 0, 0))

func get_selection():
	return level_editor.get_editor_interface().get_selection()

func refresh_item_selector_from_selected_nodes(nodes : Array):
	for node in nodes:
		var concept
		if node.is_text:
			concept = "text_" + node.concept_name
		else:
			concept = node.concept_name
		var item_id = level_editor.items_by_name.keys().find(concept)
		
		_item_container.set_item_custom_bg_color(item_id, Color(0.3, 0.7, 0.4, 0.7))
		_item_container.update()

var __portal_labels = []

func clean_portal_labels():
	for label in __portal_labels:
		label.queue_free()
	__portal_labels = []

func refresh_portals_from_selected_nodes(nodes: Array):
	for node in nodes:
		var portal_label = Label.new()
		portal_label.set_text(node.level_path)
		node.add_child(portal_label)
		__portal_labels.push_back(portal_label)

func on_selection_changed():
	clean_portal_labels()
	if not level_editor.are_all_selected_nodes_items():
		if not level_editor.are_all_selected_nodes_portals():
			return
	reset_bg_colors()
	var selection = get_selection()
	if level_editor.are_all_selected_nodes_portals():
		refresh_portals_from_selected_nodes(
			selection.get_selected_nodes()
		)
	else:
		refresh_item_selector_from_selected_nodes(
			selection.get_selected_nodes()
		)

func on_repaint_pressed():
	if not level_editor.are_all_selected_nodes_items():
		printerr("You must only select Items from the Scene editor before pressing repaint")
		return
	var selection : EditorSelection = get_selection()
	var selected_item_brush = _item_container.get_selected_items()[0]
	if not selected_item_brush:
		printerr("You must only select Items from the item selector (on the left) before pressing repaint")
		return
	print("start repainting")
	var selected_nodes = []
	for node in selection.get_selected_nodes():
		selected_nodes.push_back(node)
		var game_item = _item_container.get_item_metadata(selected_item_brush)
		print(game_item)
		node.concept_name = game_item["concept"]
		node.is_text = game_item["is_text"]
		print(node.concept_name)
		pass
	level_editor.refresh_pressed()
	call_deferred("reselect_nodes", selected_nodes)

func on_rescan_pressed():
	level_editor.index_items()
	fill_item_container()

func reselect_nodes(nodes : Array):
	var selection : EditorSelection = get_selection()
	selection.clear()
	for node in nodes:
		selection.add_node(node)
		
	
func fill_item_container():
	

	_item_container.clear()
	var i = 0
	for item_name in level_editor.items_by_name:
#		print(item_name)
		_item_container.add_item(item_name)
		_item_container.set_item_metadata(
			i,
			level_editor.items_by_name[item_name]
		)
		_item_container.set_item_icon(
			i, 
			load(
				level_editor.PATH_ITEM_DIRECTORY + "/" +
				level_editor.items_by_name[item_name]["file_name"]
			)
		)
		i += 1


func item_sizer_changed(value: float):
#	print(value)
	var icon_size = SIZE_ICON_BASE * value
	_item_container.fixed_column_width = icon_size
	_item_container.fixed_icon_size = Vector2(icon_size, icon_size)

func _enter_tree():
	_item_container = get_node("ItemListContainer/ItemContainer")
	_direction_container = get_node("ItemListContainer/DirectionContainer")
	_item_sizer = get_node("ItemSizer")
	_item_search = get_node("SearchBox")
	_repaint_button = get_node("Repaint")
	_rescan_button = get_node("Rescan")
	_item_sizer.connect("value_changed", self, "item_sizer_changed")
	
	_repaint_button.connect("pressed", self, "on_repaint_pressed")
	_rescan_button.connect("pressed", self, "on_rescan_pressed")
	
	var selection = level_editor.get_editor_interface().get_selection()
	selection.connect("selection_changed", self, "on_selection_changed")
#	fill_item_container()
