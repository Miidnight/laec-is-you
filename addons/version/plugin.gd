tool
extends EditorPlugin

func _enter_tree():
	add_autoload_singleton('Version', 'res://addons/version/Version.gd')
	add_custom_type(
		'VersionLabel',
		'Label',
		preload('version_label.gd'),
		preload('version_label.png')
	)

func _exit_tree():
	remove_autoload_singleton('Version')
	remove_custom_type('VersionLabel')
