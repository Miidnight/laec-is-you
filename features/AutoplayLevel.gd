tool
extends "res://addons/laec-is-you/entity/Level.gd"


export var inputs_sequence := "666"


var __inputs_left_to_play := Array()


signal test_passed
signal test_failed


func ready():
	.ready()
	for i in range(self.inputs_sequence.length()):
		self.__inputs_left_to_play.append(self.inputs_sequence[i])


func process(delta):
	.process(delta)
	if not Engine.editor_hint:
		process_autoplay(delta)


func process_autoplay(_delta):
	if not has_inputs_left_to_play():
		return
	if is_cooling_down_between_actions():
		return
	execute_input(pop_input())
	if not has_inputs_left_to_play():
		if is_completed():
			emit_signal("test_passed")
		else:
			emit_signal("test_failed")


func has_inputs_left_to_play():
	return not self.__inputs_left_to_play.empty()


func pop_input() -> String:
	return self.__inputs_left_to_play.pop_front()


func execute_input(input_character:String) -> void:
	if '1' == input_character:
		try_move(Directions.DOWN_LEFT)
	if '2' == input_character:
		try_move(Directions.DOWN)
	if '3' == input_character:
		try_move(Directions.DOWN_RIGHT)
	if '4' == input_character:
		try_move(Directions.LEFT)
	if '5' == input_character:
		spend_turn()
	if '6' == input_character:
		try_move(Directions.RIGHT)
	if '7' == input_character:
		try_move(Directions.UP_LEFT)
	if '8' == input_character:
		try_move(Directions.UP)
	if '9' == input_character:
		try_move(Directions.UP_RIGHT)
	if ['0', '-', '.'].has(input_character):
		undo()
	if '9' == input_character:
		try_move(Directions.UP_RIGHT)
	
	start_action_cooldown()


func show_test_passed_hint():
	pass











