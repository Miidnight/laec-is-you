extends WAT.Test

# Unit tests for sentence detection.
# Will Glomp for Pocky!

const Item = preload("res://addons/laec-is-you/entity/Item.gd")

func test_sentences() -> void:
	
	var s : Sentence
	
	var expectations = [
		{
			'sentence': [
				{'thing':    'baba'},
				{'operator': 'is'},
				{'quality':  'you'},
			],
			'subjects': ['baba'],
			'subjects_negated': [false],
			'verb': 'is',
			'complements': ['you'],
			'complements_negated': [false],
		},
		{
			'sentence': [
				{'thing':    'baba'},
				{'operator': 'and'},
				{'thing':    'france'},
				{'operator': 'is'},
				{'quality':  'you'},
			],
			'subjects': ['baba', 'france'],
			'subjects_negated': [false, false],
			'verb': 'is',
			'complements': ['you'],
			'complements_negated': [false],
		},
		{
			'sentence': [
				{'thing':    'baba'},
				{'operator': 'and'},
				{'operator': 'not'},
				{'thing':    'france'},
				{'operator': 'is'},
				{'quality':  'you'},
				{'operator': 'and'},
				{'operator': 'not'},
				{'quality':  'push'},
			],
			'subjects': ['baba', 'france'],
			'subjects_negated': [false, true],
			'verb': 'is',
			'complements': ['you', 'push'],
			'complements_negated': [false, true],
		},
		{
			'sentence': [
				{'operator': 'not'},
				{'operator': 'not'},
				{'operator': 'not'},
				{'operator': 'not'},
				{'thing':    'france'},
				{'operator': 'is'},
				{'operator': 'not'},
				{'operator': 'not'},
				{'operator': 'not'},
				{'quality':  'you'},
			],
			'subjects': ['france'],
			'subjects_negated': [false],
			'verb': 'is',
			'complements': ['you'],
			'complements_negated': [true],
		},
		{
			'sentence': [
				{'thing':    'france'},
				{'operator': 'and'},
				{'thing':    'spain'},
				{'operator': 'and'},
				{'thing':    'germany'},
				{'operator': 'and'},
				{'thing':    'italy'},
				{'operator': 'and'},
				{'operator': 'not'},
				{'thing':    'bank'},
				{'operator': 'is'},
				{'quality':  'win'},
			],
			'subjects': ['france', 'spain', 'germany', 'italy', 'bank'],
			'subjects_negated': [false, false, false, false, true],
			'verb': 'is',
			'complements': ['win'],
			'complements_negated': [false],
		},
	]
	
	for expectation in expectations:
		s = Sentence.new()
		var items := Array()
		for word in expectation['sentence']:
			var item = Item.new()
			item.is_text = true
			if word.has('thing'):
				item.thing_name = word['thing']
			if word.has('operator'):
				item.operator_name = word['operator']
			if word.has('quality'):
				item.quality_name = word['quality']
			items.append(item)
		s.from_items(items)
		asserts.is_true(s.is_valid, "Sentence `%s` is valid." % [expectation['sentence']])
		if s.is_valid:
			var actual_subjects := Array()
			var actual_subjects_negated := Array()
			for subject in s.subjects:
				actual_subjects.append(subject.concept_name)
				actual_subjects_negated.append(subject.negated)
			asserts.is_equal(expectation['subjects'], actual_subjects, "Subjects are detected.")
			asserts.is_equal(expectation['subjects_negated'], actual_subjects_negated, "Subjects' negations are detected.")
			asserts.is_equal(s.verb.concept_name, expectation['verb'], "Verb is detected.")
			var actual_complements := Array()
			var actual_complements_negated := Array()
			for complement in s.complements:
				actual_complements.append(complement.concept_name)
				actual_complements_negated.append(complement.negated)
			asserts.is_equal(expectation['complements'], actual_complements, "Complements are detected.")
			asserts.is_equal(expectation['complements_negated'], actual_complements_negated, "Complements' negations are detected.")

