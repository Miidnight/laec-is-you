extends Node


# 
# Handles Application logic.
# 
# - Window
#   - Closing
#   - Fullscreen Toggle
# - Settings
# - …
# 
# Meant to be used as a Godot Singleton.
# 


# __          ___           _
# \ \        / (_)         | |
#  \ \  /\  / / _ _ __   __| | _____      __
#   \ \/  \/ / | | '_ \ / _` |/ _ \ \ /\ / /
#    \  /\  /  | | | | | (_| | (_) \ V  V /
#     \/  \/   |_|_| |_|\__,_|\___/ \_/\_/
#


func quit() -> void:
	get_tree().quit()


func exit() -> void:
	quit()


func is_fullscreen() -> bool:
	return OS.window_fullscreen


func toggle_fullscreen() -> void:
	OS.window_fullscreen = not OS.window_fullscreen


#   _____      _   _   _
#  / ____|    | | | | (_)
# | (___   ___| |_| |_ _ _ __   __ _ ___
#  \___ \ / _ \ __| __| | '_ \ / _` / __|
#  ____) |  __/ |_| |_| | | | | (_| \__ \
# |_____/ \___|\__|\__|_|_| |_|\__, |___/
#                               __/ |
#                              |___/

signal setting_changed


const SETTINGS_FILEPATH := "user://settings.cfg"

# Meta structure proposal for UI generation.
# Only the "default" property is used for now.
# Add more using add_settings_meta() or just edit those :(|)
var __settings_meta : Dictionary = {
	"audio": {
#		"muted": {
#			"type": "toggle", # or "checkbox"
#			"default": false,
#		},
		"main_volume": {
#			"type": "horizontal_slider",
#			"minimum": 0,
#			"maximum": 100,
#			"step": 1,
			"default": 42,
		},
	},
}


var __settings : ConfigFile


func _init():
	var loaded = load_settings()
	if OK != loaded:
		printerr("ERROR: Settings failed to load!")
	apply_known_settings()  # nonetheless


func load_settings() -> int:  # OK|ERR_XXX
	__settings = ConfigFile.new()
	var report = __settings.load(SETTINGS_FILEPATH)
	
	if OK == report:
		for section in __settings_meta:
			for key in __settings_meta[section]:
				if not __settings.has_section_key(section, key):
					__settings.set_value(section, key, __settings_meta[section][key]["default"])
	else:
		for section in __settings_meta:
			for key in __settings_meta[section]:
				__settings.set_value(section, key, __settings_meta[section][key]["default"])
		report = __settings.save(SETTINGS_FILEPATH)
		if OK != report:
			printerr("Failed to write to the settings file at `%s'." % [SETTINGS_FILEPATH])
	
	return report


func apply_known_settings():
	adjust_main_volume_from_settings()


func add_settings_meta(settings_meta:Dictionary):
	merge_dict(__settings_meta, settings_meta)
	load_settings()
	apply_known_settings()


func set_setting(setting_section, setting_key, setting_value) -> void:
	self.__settings.set_value(setting_section, setting_key, setting_value)
	var saved = self.__settings.save(SETTINGS_FILEPATH)
	if OK != saved:
		printerr("Failed to write settings file at `'." % SETTINGS_FILEPATH)
	emit_signal("setting_changed", setting_section, setting_key, setting_value)


func get_setting(setting_section, setting_key, default_value=null):
	return self.__settings.get_value(setting_section, setting_key, default_value)


func adjust_main_volume_from_settings():
	#prints("Main Volume", get_setting('audio', 'main_volume', 100))
	var db_volume = setting_to_db(get_setting('audio', 'main_volume', 100))
	AudioServer.set_bus_volume_db(
		AudioServer.get_bus_index("Master"),
		db_volume
	)


static func setting_to_db(setting_value):
	# Custom range, could perhaps be redone with linear2db()
	return setting_value * 0.6 - 60.0


#   _____           _
#  / ____|         | |
# | (___  _   _ ___| |_ ___ _ __ ___
#  \___ \| | | / __| __/ _ \ '_ ` _ \
#  ____) | |_| \__ \ ||  __/ | | | | |
# |_____/ \__, |___/\__\___|_| |_| |_|
#          __/ |
#         |___/


func get_unique_device_id():
	return OS.get_unique_id()


#  _    _ _   _ _
# | |  | | | (_) |
# | |  | | |_ _| |___
# | |  | | __| | / __|
# | |__| | |_| | \__ \
#  \____/ \__|_|_|___/
#
#


# https://godotengine.org/qa/8024/update-dictionary-method
static func merge_dict(dest, source):
	for key in source:
		if dest.has(key):
			var dest_value = dest[key]
			var source_value = source[key]
			if typeof(dest_value) == TYPE_DICTIONARY:
				if typeof(source_value) == TYPE_DICTIONARY:
					merge_dict(dest_value, source_value)
				else:
					dest[key] = source_value
			else:
				dest[key] = source_value
		else:
			dest[key] = source[key]


