tool
extends Node


var sprites_directory = "res://sprites"

var undefined = "undefined"


func get_for_concept(concept:String, is_text:=false, subdir:='items') -> SpriteFrames:
	return make_for_concept(concept, is_text, subdir)


func make_for_concept(concept:String, is_text:=false, subdir:='items') -> SpriteFrames:
	var sf := SpriteFrames.new()
	var file := File.new()
	if is_text:
		# Here we could use `-` instead of `_` to avoid collisions,
		# at the expense of less mnemonic (uglier!) file names.
		# Don't name a concept `text`, and we may keep homogenous file names.
		concept = "text_%s" % [concept]
	
	for direction in Directions.ALL.keys():
		var more := true
		var state := 0
		while more:
			var animation := "%s_%d" % [direction, state]
			
			sf.add_animation(animation)
			
			for shiver in range(3):
				var frame_filename := "%s/%s/%s_%s_%d.png" % [
					sprites_directory, subdir, concept, animation, shiver
				]
				if not file.file_exists(frame_filename):
#					print("Nothing found at `%s`…" % frame_filename)
					frame_filename = "%s/%s/%s_%s_%d.png" % [
						sprites_directory, subdir, concept, direction, shiver,
					]
					more = false
				if not file.file_exists(frame_filename):
#					print("Nothing found at `%s`…" % frame_filename)
					frame_filename = "%s/%s/%s_%d.png" % [
						sprites_directory, subdir, concept, shiver,
					]
					more = false
				if not file.file_exists(frame_filename):
					frame_filename = "%s/%s/%s_%d.png" % [
						sprites_directory, subdir, self.undefined, shiver,
					]
#					printerr("Missing sprite for `%s` (animation=%s) (state=%d) (filename=%s)." % [
#						concept, animation, state, frame_filename,
#					])
#					break
				
#				print("Adding frame `%s`…" % frame_filename)
				sf.add_frame(animation, load(frame_filename))
			
			if state > 0 and not more:
				sf.remove_animation(animation)
			
			state += 1
		
	
	return sf

