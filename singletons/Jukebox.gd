extends AudioStreamPlayer


# The Jukebox is to do.
# Feel like it?  Do it!

# Specs
# -----
# 
# - Handles multiple tracks
# - Handles limbo alternative
# - Handles ambiance playlists ?


func play(from_position := 0.0):
	if not is_playing():
		.play(from_position)

