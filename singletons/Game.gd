extends Node

# A Global Singleton for Game Logic.
# 
# Handles
# -------
# - Orchestration of top-level scenes (mostly levels)
# - Transitions between scenes
# - Save file
# - …
# 
# When relevant, try to shift those responsibilities
# to other singletons, or plain objects if you can.
# This is why not all the properties of Game
# are at the top of this file, but along with the code.
# 
# This is a useful place to blackboard.
# 


# Adding new effects can be done in `effects/transitions.shader`.
# Add a new bool uniform named `is_<something>` like `is_snake` for example,
# add the GLSL shader code that handles it, add it here and enjoy!
var available_transition_effects = [
	'fade',
	'checkerboard',
	'vertical_stripes',
	'horizontal_stripes',
	'dots',
	'clock',
	# Ideas
	#'diagonal_stripes',
	#'hive',
	#'sprite_zoom',
]

var transitions_duration = 0.42;
var transitions_pause_duration = 0.70;

################################################################################


const ItemScene = preload("res://core/Item.tscn")
#const LevelScene = preload("res://core/Level.tscn")
const LevelVictoryScreen = preload("res://menus/LevelVictoryScreen.tscn")


var root : Viewport  # sugar

# The game should be deterministic,
# but transitions are randomized.
var rng = RandomNumberGenerator.new()


func _ready():
	self.root = get_tree().get_root()
	load_game()
	setup_transition_effect_layer()
	# Grab startup scene, defined in Project settings
	self.current_scene = root.get_child(root.get_child_count() - 1)


func _process(_delta):
	update_transitions_shader_time()


func _input(_event):
	if Input.is_action_just_pressed("toggle_fullscreen"):
		App.toggle_fullscreen()




#   ____           _               _
#  / __ \         | |             | |
# | |  | |_ __ ___| |__   ___  ___| |_ _ __ __ _
# | |  | | '__/ __| '_ \ / _ \/ __| __| '__/ _` |
# | |__| | | | (__| | | |  __/\__ \ |_| | | (_| |
#  \____/|_|  \___|_| |_|\___||___/\__|_|  \__,_|
#
#

var current_scene = null
var scenes_ancestry := Array()  # of Node (scenes)
var __is_currently_switching := false


func switch_to_level(level_path, free_current:=false, append_ancestry:=true):
	switch_to_scene_path(level_path, free_current, append_ancestry)


func switch_to_scene_path(level_path, free_current:=false, append_ancestry:=true):
	var level_scene = ResourceLoader.load(level_path)
	assert(level_scene, "Failed to load scene at `%s'." % [level_path])
	level_scene = level_scene.instance()
	assert(level_scene, "Failed to instantiate scene at `%s'." % [level_path])
	switch_to_scene(level_scene, free_current, append_ancestry)


func switch_to_scene(level_scene, free_current:=false, append_ancestry:=true):
	
	if self.__is_currently_switching:
		return
	self.__is_currently_switching = true

	if self.current_scene and append_ancestry:
		#prints("Appending to ancestry", self.current_scene)
		self.scenes_ancestry.append(self.current_scene)

	var duration = transitions_duration
	start_transition_effect(duration)
	yield(get_tree().create_timer(
		duration + transitions_pause_duration
	), "timeout")
	
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.
	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:
	call_deferred("__deferred_switch_to_scene", level_scene, free_current)
	
	start_transition_effect(duration, true)


func __deferred_switch_to_scene(level_scene, free_current):
	# Load the new scene.
#	var level_scene = ResourceLoader.load(level_path)

	if not level_scene:
		printerr("There is no scene to switch to.")
		return
	
	# It is now safe to remove the current scene
	if free_current:
		prints("Freeing current scene", self.current_scene)
		self.current_scene.free()
	else:
		self.current_scene.get_parent().remove_child(current_scene)
		# Will be garbage-collected anyway, unless added to ancestry

	# Register the new current scene
	self.current_scene = level_scene

	# Add it to the active scene, as child of root.
	get_tree().get_root().add_child(self.current_scene)

	# Make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(self.current_scene)

	self.__is_currently_switching = false


#  _                    _
# | |                  | |
# | |     _____   _____| |___
# | |    / _ \ \ / / _ \ / __|
# | |___|  __/\ V /  __/ \__ \
# |______\___| \_/ \___|_|___/
#
#

func reset_level():
	var current_scene_path = self.current_scene.filename
	if not current_scene_path:
		printerr(
			"Cannot reset current level because " +
			"`switch_to_level(…)' was never called."
		)
		return
	self.switch_to_level(current_scene_path, true, false)


func enter_level(level_path):
	switch_to_level(level_path)


func go_to_level_victory_screen(for_level) -> void:
	var levelVictoryScreen = LevelVictoryScreen.instance()
	levelVictoryScreen.prepare(for_level)
	assert(levelVictoryScreen.level)
	switch_to_scene(levelVictoryScreen, false, false)


#  _   _             _             _   _
# | \ | |           (_)           | | (_)
# |  \| | __ ___   ___  __ _  __ _| |_ _  ___  _ __
# | . ` |/ _` \ \ / / |/ _` |/ _` | __| |/ _ \| '_ \
# | |\  | (_| |\ V /| | (_| | (_| | |_| | (_) | | | |
# |_| \_|\__,_| \_/ |_|\__, |\__,_|\__|_|\___/|_| |_|
#                       __/ |
#                      |___/


func go_back_to_map() -> void:
	go_back()


func go_back() -> void:
	if not self.scenes_ancestry:
		#printerr("Nothing to go back to.")
		App.exit()
		return
	
	var previous_scene = self.scenes_ancestry.pop_back()
	
	switch_to_level(previous_scene.filename, true, false)


#  _______                  _ _   _
# |__   __|                (_) | (_)
#    | |_ __ __ _ _ __  ___ _| |_ _  ___  _ __  ___
#    | | '__/ _` | '_ \/ __| | __| |/ _ \| '_ \/ __|
#    | | | | (_| | | | \__ \ | |_| | (_) | | | \__ \
#    |_|_|  \__,_|_| |_|___/_|\__|_|\___/|_| |_|___/
#
#

var transition_effects : ColorRect
var transition_effects_layer : CanvasLayer


func setup_transition_effect_layer():
	var layer := CanvasLayer.new()
	layer.layer = 10
	var fx_rect := ColorRect.new()
	fx_rect.anchor_left = 0.0
	fx_rect.anchor_top = 0.0
	fx_rect.anchor_right = 1.0
	fx_rect.anchor_bottom = 1.0
	fx_rect.mouse_filter = fx_rect.MOUSE_FILTER_IGNORE
	
	var layer_material := ShaderMaterial.new()
	var layer_shader = preload("res://effects/transition.shader")
	layer_material.set_shader(layer_shader)
	fx_rect.set_material(layer_material)
	
	layer.add_child(fx_rect)
	self.add_child(layer)
	
	self.transition_effects = fx_rect
	self.transition_effects_layer = layer
	
	rng.randomize()


func start_transition_effect(duration:float, fade_out:=false, effect:='random', options:={}):
	if 'random' == effect:
		effect = available_transition_effects[(
			rng.randi() % available_transition_effects.size()
		)]
	
	var material = self.transition_effects.get_material()
	material.set_shader_param('duration_ms', duration * 1000.0)
	material.set_shader_param('is_fade_out', fade_out)
	material.set_shader_param('fade_color', options.get('fade_color', Color.black))
	
	for any_effect in available_transition_effects:
		material.set_shader_param("is_%s" % any_effect, false)
	material.set_shader_param("is_%s" % effect, true)
	
	material.set_shader_param('is_reversed', options.get('is_reversed', get_random_bool()))
	material.set_shader_param('is_centered', options.get('is_centered', get_random_bool()))
	material.set_shader_param('is_flip_h', options.get('is_flip_h', get_random_bool()))
	material.set_shader_param('is_flip_v', options.get('is_flip_v', get_random_bool()))
	
	material.set_shader_param('cell_size', options.get('is_flip_v', rng.randi_range(7,111)))
	
	# Lastly, …
	update_transitions_shader_time(true)
	material.set_shader_param('is_playing', true)


func update_transitions_shader_time(start_too:=false) -> void:
	var material = self.transition_effects.get_material()
	var rollover = 3600 * 1000  # mimic TIME's buffer overflow avoidance0
	var time_ms = OS.get_ticks_msec() % rollover
	
	if start_too:
		material.set_shader_param('start_time_ms', time_ms)
	material.set_shader_param('current_time_ms', time_ms)


func get_random_bool():
	# RNG is pretty recent in Godot
	# If anyone feels like writing an extension of `RandomNumberGenerator`
	# with the goodies we need, and then perhaps make a plugin out of it, go!
	# rng.randb()
	# I'm not overly fond of the randX API
	return true if rng.randi() % 2 == 0 else false


#   _____
#  / ____|
# | (___   __ ___   _____
#  \___ \ / _` \ \ / / _ \
#  ____) | (_| |\ V /  __/
# |_____/ \__,_| \_/ \___|
#
#
# Current structure of the saved data:
# {
#   'levels': {
#     '<level_filepath>': {
#       'complete': <bool>,
#     },
#   },
# }
# 

var save_path := "user://savegame_01.save"
#var save_paths := Array()
var current_save_data := Dictionary()


func register_victory():
	var current_scene_path = self.current_scene.filename
	if not current_save_data.has('levels'):
		current_save_data['levels'] = Dictionary()
	current_save_data['levels'][current_scene_path] = {
		'complete': true,
		#'best_time': ?
	}
	save_game()


func save_game():
	var save_data_json = to_json(self.current_save_data)
	var save_game = File.new()
	save_game.open(self.save_path, File.WRITE)
	save_game.store_line(save_data_json)
	save_game.close()


func load_game():
	var save_game = File.new()
	if not save_game.file_exists(self.save_path):
		print("[Game] No save to load at `%s'." % self.save_path)
		return  # We don't have a save to load.

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open(self.save_path, File.READ)
	self.current_save_data = parse_json(save_game.get_line())
	save_game.close()


func is_level_complete(level_path):
	return (
		self.current_save_data.has('levels')
		and self.current_save_data['levels'].has(level_path)
		and self.current_save_data['levels'][level_path]['complete']
	)


#   _____
#  / ____|
# | (___  _ __   __ ___      ___ __
#  \___ \| '_ \ / _` \ \ /\ / / '_ \
#  ____) | |_) | (_| |\ V  V /| | | |
# |_____/| .__/ \__,_| \_/\_/ |_| |_|
#        | |
#        |_|

func spawn_item(item_to_copy=null, extra_properties:Dictionary={}):
	"""
	Creates a new Item instance and adds it to the scene tree.
	This may also emit some signals in the future.
	
	item_to_copy:
		Another Item to copy from, if provided.
		Has no type set (Item) in the definition above
		because of cyclic failures with class_name.
	extra_properties:
		Takes the form of a pickled item, basically.
		Has priority over the item to copy and defaults.
	"""
	if not self.current_scene:
		printerr("No current scene into which spawn an Item.")
		return
	if not self.current_scene.items_layer:
		printerr("No items layer into which spawn an Item.")
		return
	
	var item = ItemScene.instance()
	if item_to_copy:
		# Perhaps move the copy logic in Item
		item.position = item_to_copy.position
		item.cell_position = item_to_copy.cell_position
		item.concept_name = item_to_copy.concept_name
		item.direction = item_to_copy.direction
		item.is_text = item_to_copy.is_text
		######################################
		# Shouldn't we also copy the flags?
		######################################
	
	for prop in extra_properties:
		item.set(prop, extra_properties[prop])
	
	self.current_scene.items_layer.add_child(item)
	
	return item




