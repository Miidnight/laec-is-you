#!/bin/sh

# Helps creating the gettext template file.

# Install:
#     pip install babel babel-godot

pybabel extract \
	-F .babelrc \
	-k text \
	-k LineEdit/placeholder_text \
	-k tr \
	-o translations/godot-l10n.pot \
	.
